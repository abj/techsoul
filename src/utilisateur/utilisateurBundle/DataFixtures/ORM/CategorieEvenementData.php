<?php /*
namespace utilisateur\utilisateurBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use utilisateur\utilisateurBundle\Entity\CategorieEvenement;

class CategorieEvenementData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $CategorieEvenement = new CategorieEvenement();
        $CategorieEvenement->setCategorie('Web Design');
        $manager->persist($CategorieEvenement);
        
        
        
        $CategorieEvenement2 = new CategorieEvenement();
        $CategorieEvenement2->setCategorie('Web Developpement'); 
        $manager->persist($CategorieEvenement2);
       
       
        $CategorieEvenement3 = new CategorieEvenement();
        $CategorieEvenement3->setCategorie('Sport'); 
        $manager->persist($CategorieEvenement3);
       
       
        $CategorieEvenement4 = new CategorieEvenement();
        $CategorieEvenement4->setCategorie('Nature et envirenement'); 
        $manager->persist($CategorieEvenement4);
       
       
       
       
       
        $CategorieEvenement5 = new CategorieEvenement();
        $CategorieEvenement5->setCategorie('reparation  informatique'); 
        $manager->persist($CategorieEvenement5);
       
       
       
        $CategorieEvenement6 = new CategorieEvenement();
        $CategorieEvenement6->setCategorie('Mobile Developement'); 
        $manager->persist($CategorieEvenement6);

        
        
        
        
        
        $CategorieEvenement7 = new CategorieEvenement();
        $CategorieEvenement7->setCategorie('Bricolage'); 
        $manager->persist($CategorieEvenement7);
       
       
        
        
        
        $CategorieEvenement8 = new CategorieEvenement();
        $CategorieEvenement8->setCategorie('nouvelle technologie'); 
        $manager->persist($CategorieEvenement8);

        
        
        
       
        $CategorieEvenement9 = new CategorieEvenement();
        $CategorieEvenement9->setCategorie('Divertissement'); 
        $manager->persist($CategorieEvenement9);
       
       
       
        $CategorieEvenement10 = new CategorieEvenement();
        $CategorieEvenement10->setCategorie('humanitaire'); 
        $manager->persist($CategorieEvenement10);
       
       
       
       
       
       
       
       
       
       
       
       
        $manager->flush();
        
        
   
         $this->addReference('CategorieEvenement', $CategorieEvenement);
         $this->addReference('CategorieEvenement2', $CategorieEvenement2);
         $this->addReference('CategorieEvenement3', $CategorieEvenement2);
         $this->addReference('CategorieEvenement4', $CategorieEvenement2);
         $this->addReference('CategorieEvenement5', $CategorieEvenement2);
         $this->addReference('CategorieEvenement6', $CategorieEvenement2);
         $this->addReference('CategorieEvenement7', $CategorieEvenement2);
         $this->addReference('CategorieEvenement8', $CategorieEvenement2);
         $this->addReference('CategorieEvenement9', $CategorieEvenement2);
         $this->addReference('CategorieEvenement10', $CategorieEvenement2);
         
        }
         public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 4;
    }
    
}