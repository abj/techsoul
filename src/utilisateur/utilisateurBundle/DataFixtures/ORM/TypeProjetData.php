<?php
namespace utilisateur\utilisateurBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use utilisateur\utilisateurBundle\Entity\TypeProjet;

class TypeProjetData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $TypeProjet = new TypeProjet();
        $TypeProjet->setType('Charité');
        $manager->persist($TypeProjet);
        
        
        
        $TypeProjet2 = new TypeProjet();
        $TypeProjet2->setType('à fin personnel'); 
        $manager->persist($TypeProjet2);
        

        $manager->persist($TypeProjet2);
        $manager->flush();
        
        
        
         $this->addReference('TypeProjet', $TypeProjet);
         $this->addReference('TypeProjet2', $TypeProjet2);
        }
         public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
    
}