<?php

namespace utilisateur\utilisateurBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use FOS\CommentBundle\Entity\Comment as BaseComment;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet", indexes={@ORM\Index(name="IDX_50159CA9F97A9A35", columns={"ID_TYPE"}), @ORM\Index(name="IDX_50159CA9F514AB27", columns={"ID_CATEGORIE_PROJET"}), @ORM\Index(name="IDX_50159CA95734C0BD", columns={"ID_INVESTISSEMENT"}), @ORM\Index(name="IDX_50159CA9BF396750", columns={"id"})})
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks 
 */
class Projet extends BaseComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_PROJET", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     */
    protected $idProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="NOM_PROJET", type="string", length=255, nullable=true)
     */
    protected $nomProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="RESUME", type="text", nullable=true)
     */
    protected $resume;
    /**
     * @var string
     * @Vich\Uploadable
     * @ORM\Column(name="image", type="text", nullable=true)
     * @Assert\Image(maxSize="1000000")
     * @Vich\UploadableField(mapping="Projet_file", fileNameProperty="imageFile", nullable=true)
     */
    protected $image;

    /**
     * @var float
     *
     * @ORM\Column(name="BUDJET", type="float", precision=10, scale=0, nullable=true)
     */
    protected $budjet;
    /**
     * @var float
     *
     * @ORM\Column(name="argent", type="float", precision=10, scale=0, nullable=true)
     */
    protected $argent;
    /**
     * @var string
     * 
     * @ORM\Column(name="imageFile", type="text", nullable=true)
     * 
     */
    protected $imageFile;


    /**
     * @var string
     * @Vich\Uploadable
     * @ORM\Column(name="FICHIER", type="text", nullable=true)
     * @Assert\Image(maxSize="100000")
     * @Vich\UploadableField(mapping="Projet_image", fileNameProperty="imageFile", nullable=true)
     */
    protected $fichier;

    /**
     * @var \utilisateur\utilisateurBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="utilisateur\utilisateurBundle\Entity\User")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     * })
     */
    protected $id;

    /**
     * @var \utilisateur\utilisateurBundle\Entity\Investissement
     *
     * @ORM\ManyToOne(targetEntity="utilisateur\utilisateurBundle\Entity\Investissement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_INVESTISSEMENT", referencedColumnName="ID_INVESTISSEMENT")
     * })
     */
    protected $idInvestissement;

    /**
     * @var \utilisateur\utilisateurBundle\Entity\CategorieProjet
     *
     * @ORM\ManyToOne(targetEntity="utilisateur\utilisateurBundle\Entity\CategorieProjet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_CATEGORIE_PROJET", referencedColumnName="ID_CATEGORIE_PROJET")
     * })
     */
    protected $idCategorieProjet;

    /**
     * @var \utilisateur\utilisateurBundle\Entity\TypeProjet
     *
     * @ORM\ManyToOne(targetEntity="utilisateur\utilisateurBundle\Entity\TypeProjet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_TYPE", referencedColumnName="ID_TYPE")
     * })
     */
    protected $idType;


    
    
    
    
    

    /**
     * Get idProjet
     *
     * @return integer 
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }

    /**
     * Set nomProjet
     *
     * @param string $nomProjet
     * @return Projet
     */
    public function setNomProjet($nomProjet)
    {
        $this->nomProjet = $nomProjet;

        return $this;
    }

    /**
     * Get nomProjet
     *
     * @return string 
     */
    public function getNomProjet()
    {
        return $this->nomProjet;
    }

    /**
     * Set resume
     *
     * @param string $resume
     * @return Projet
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string 
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Projet
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set budjet
     *
     * @param float $budjet
     * @return Projet
     */
    public function setBudjet($budjet)
    {
        $this->budjet = $budjet;

        return $this;
    }

    /**
     * Get budjet
     *
     * @return float 
     */
    public function getBudjet()
    {
        return $this->budjet;
    }

    /**
     * Set fichier
     *
     * @param string $fichier
     * @return Projet
     */
    public function setFichier($fichier)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * Get fichier
     *
     * @return string 
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * Set id
     *
     * @param \utilisateur\utilisateurBundle\Entity\User $id
     * @return Projet
     */
    public function setId(\utilisateur\utilisateurBundle\Entity\User $id = null)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \utilisateur\utilisateurBundle\Entity\User 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idInvestissement
     *
     * @param \utilisateur\utilisateurBundle\Entity\Investissement $idInvestissement
     * @return Projet
     */
    public function setIdInvestissement(\utilisateur\utilisateurBundle\Entity\Investissement $idInvestissement = null)
    {
        $this->idInvestissement = $idInvestissement;

        return $this;
    }

    /**
     * Get idInvestissement
     *
     * @return \utilisateur\utilisateurBundle\Entity\Investissement 
     */
    public function getIdInvestissement()
    {
        return $this->idInvestissement;
    }

    /**
     * Set idCategorieProjet
     *
     * @param \utilisateur\utilisateurBundle\Entity\CategorieProjet $idCategorieProjet
     * @return Projet
     */
    public function setIdCategorieProjet(\utilisateur\utilisateurBundle\Entity\CategorieProjet $idCategorieProjet = null)
    {
        $this->idCategorieProjet = $idCategorieProjet;

        return $this;
    }

    /**
     * Get idCategorieProjet
     *
     * @return \utilisateur\utilisateurBundle\Entity\CategorieProjet 
     */
    public function getIdCategorieProjet()
    {
        return $this->idCategorieProjet;
    }

    /**
     * Set idType
     *
     * @param \utilisateur\utilisateurBundle\Entity\TypeProjet $idType
     * @return Projet
     */
    public function setIdType(\utilisateur\utilisateurBundle\Entity\TypeProjet $idType = null)
    {
        $this->idType = $idType;

        return $this;
    }

    /**
     * Get idType
     *
     * @return \utilisateur\utilisateurBundle\Entity\TypeProjet 
     */
    public function getIdType()
    {
        return $this->idType;
    }
    function getImageFile() {
        return $this->imageFile;
    }

    function setImageFile($imageFile) {
        $this->imageFile = $imageFile;
    }


    function getArgent() {
        return $this->argent;
    }

    function setArgent($argent) {
        $this->argent = $argent;
    }


    
    
    
    
}
