<?php

/* crowdBundle:Default/page:acceuil.html.twig */
class __TwigTemplate_3a9d2d6e89477b8753105ab4574a8577ed03697b8f009785668716d71b813b8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "crowdBundle:Default/page:acceuil.html.twig", 1);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "
  
        ";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "             
             
                     <!-- Homepage Slider -->
        <div class=\"homepage-slider\">
        \t<div id=\"sequence\">
\t\t\t\t<ul class=\"sequence-canvas\">
\t\t\t\t\t<!-- Slide 1 -->
\t\t\t\t\t<li class=\"bg4\">
\t\t\t\t\t\t<!-- Slide Title -->
\t\t\t\t\t\t<h2 class=\"title\">Responsive</h2>
\t\t\t\t\t\t<!-- Slide Text -->
\t\t\t\t\t\t<h3 class=\"subtitle\">It looks great on desktops, laptops, tablets and smartphones</h3>
\t\t\t\t\t\t<!-- Slide Image -->
\t\t\t\t\t\t<img class=\"slide-img\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/homepage-slider/slide1.png"), "html", null, true);
        echo "\" alt=\"Slide 1\" />
\t\t\t\t\t</li>
\t\t\t\t\t<!-- End Slide 1 -->
\t\t\t\t\t<!-- Slide 2 -->
\t\t\t\t\t<li class=\"bg3\">
\t\t\t\t\t\t<!-- Slide Title -->
\t\t\t\t\t\t<h2 class=\"title\">Color Schemes</h2>
\t\t\t\t\t\t<!-- Slide Text -->
\t\t\t\t\t\t<h3 class=\"subtitle\">Comes with 5 color schemes and it's easy to make your own!</h3>
\t\t\t\t\t\t<!-- Slide Image -->
\t\t\t\t\t\t<img class=\"slide-img\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/homepage-slider/slide2.png"), "html", null, true);
        echo "\" alt=\"Slide 2\" />
\t\t\t\t\t</li>
\t\t\t\t\t<!-- End Slide 2 -->
\t\t\t\t\t<!-- Slide 3 -->
\t\t\t\t\t<li class=\"bg1\">
\t\t\t\t\t\t<!-- Slide Title -->
\t\t\t\t\t\t<h2 class=\"title\">Feature Rich</h2>
\t\t\t\t\t\t<!-- Slide Text -->
\t\t\t\t\t\t<h3 class=\"subtitle\">Huge amount of components and over 30 sample pages!</h3>
\t\t\t\t\t\t<!-- Slide Image -->
\t\t\t\t\t\t<img class=\"slide-img\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/homepage-slider/slide3.png"), "html", null, true);
        echo "\" alt=\"Slide 3\" />
\t\t\t\t\t</li>
\t\t\t\t\t<!-- End Slide 3 -->
\t\t\t\t</ul>
\t\t\t\t<div class=\"sequence-pagination-wrapper\">
\t\t\t\t\t<ul class=\"sequence-pagination\">
\t\t\t\t\t\t<li>1</li>
\t\t\t\t\t\t<li>2</li>
\t\t\t\t\t\t<li>3</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
        </div>
        <!-- End Homepage Slider -->

\t\t<!-- Press Coverage -->
        <div class=\"section\">
\t    \t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-4 col-sm-6\">
\t\t\t\t\t\t<div class=\"in-press press-wired\">
\t\t\t\t\t\t\t<a href=\"#\">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4 col-sm-6\">
\t\t\t\t\t\t<div class=\"in-press press-mashable\">
\t\t\t\t\t\t\t<a href=\"#\">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4 col-sm-6\">
\t\t\t\t\t\t<div class=\"in-press press-techcrunch\">
\t\t\t\t\t\t\t<a href=\"#\">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<!-- Press Coverage -->

\t\t<!-- Services -->
        <div class=\"section\">
\t        <div class=\"container\">
\t        \t<div class=\"row\">
\t        \t\t<div class=\"col-md-4 col-sm-6\">
\t        \t\t\t<div class=\"service-wrapper\">
\t\t        \t\t\t<img src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/service-icon/diamond.png"), "html", null, true);
        echo "\" alt=\"Service 1\">
\t\t        \t\t\t<h3>Aliquam in adipiscing</h3>
\t\t        \t\t\t<p>Praesent rhoncus mauris ac sollicitudin vehicula. Nam fringilla turpis turpis, at posuere turpis aliquet sit amet condimentum</p>
\t\t        \t\t\t<a href=\"#\" class=\"btn\">Read more</a>
\t\t        \t\t</div>
\t        \t\t</div>
\t        \t\t<div class=\"col-md-4 col-sm-6\">
\t        \t\t\t<div class=\"service-wrapper\">
\t\t        \t\t\t<img src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/service-icon/ruler.png"), "html", null, true);
        echo "\" alt=\"Service 2\">
\t\t        \t\t\t<h3>Curabitur mollis</h3>
\t\t        \t\t\t<p>Suspendisse eget libero mi. Fusce ligula orci, vulputate nec elit ultrices, ornare faucibus orci. Aenean lectus sapien, vehicula</p>
\t\t        \t\t\t<a href=\"#\" class=\"btn\">Read more</a>
\t\t        \t\t</div>
\t        \t\t</div>
\t        \t\t<div class=\"col-md-4 col-sm-6\">
\t        \t\t\t<div class=\"service-wrapper\">
\t\t        \t\t\t<img src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/service-icon/box.png"), "html", null, true);
        echo "\" alt=\"Service 3\">
\t\t        \t\t\t<h3>Vivamus mattis</h3>
\t\t        \t\t\t<p>Phasellus posuere et nisl ac commodo. Nulla facilisi. Sed tincidunt bibendum cursus. Aenean vulputate aliquam risus rutrum scelerisque</p>
\t\t        \t\t\t<a href=\"#\" class=\"btn\">Read more</a>
\t\t        \t\t</div>
\t        \t\t</div>
\t        \t</div>
\t        </div>
\t    </div>
\t    <!-- End Services -->

\t\t<!-- Call to Action Bar -->
\t    <div class=\"section section-white\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"calltoaction-wrapper\">
\t\t\t\t\t\t\t<h3>It's a free multipurpose Bootstrap 3 template!</h3> <a href=\"http://www.dragdropsite.com\" class=\"btn btn-orange\">Download here!</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<!-- End Call to Action Bar -->

\t\t<!-- Testimonials -->
\t    <div class=\"section\">
\t\t\t<div class=\"container\">
\t\t\t\t<h2>Testimonials</h2>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<!-- Testimonial -->
\t\t\t\t\t<div class=\"testimonial col-md-4 col-sm-6\">
\t\t\t\t\t\t<!-- Author Photo -->
\t\t\t\t\t\t<div class=\"author-photo\">
\t\t\t\t\t\t\t<img src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/user1.jpg"), "html", null, true);
        echo "\" alt=\"Author 1\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"testimonial-bubble\">
\t\t\t\t\t\t\t<blockquote>
\t\t\t\t\t\t\t\t<!-- Quote -->
\t\t\t\t\t\t\t\t<p class=\"quote\">
\t\t                            \"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.\"
                        \t\t</p>
                        \t\t<!-- Author Info -->
                        \t\t<cite class=\"author-info\">
                        \t\t\t- Name Surname,<br>Managing Director at <a href=\"#\">Some Company</a>
                        \t\t</cite>
                        \t</blockquote>
                        \t<div class=\"sprite arrow-speech-bubble\"></div>
                        </div>
                    </div>
                    <!-- End Testimonial -->
                    <div class=\"testimonial col-md-4 col-sm-6\">
\t\t\t\t\t\t<div class=\"author-photo\">
\t\t\t\t\t\t\t<img src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/user5.jpg"), "html", null, true);
        echo "\" alt=\"Author 2\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"testimonial-bubble\">
\t\t\t\t\t\t\t<blockquote>
\t\t\t\t\t\t\t\t<p class=\"quote\">
\t\t                            \"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.\"
                        \t\t</p>
                        \t\t<cite class=\"author-info\">
                        \t\t\t- Name Surname,<br>Managing Director at <a href=\"#\">Some Company</a>
                        \t\t</cite>
                        \t</blockquote>
                        \t<div class=\"sprite arrow-speech-bubble\"></div>
                        </div>
                    </div>
\t\t\t\t\t<div class=\"testimonial col-md-4 col-sm-6\">
\t\t\t\t\t\t<div class=\"author-photo\">
\t\t\t\t\t\t\t<img src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/user2.jpg"), "html", null, true);
        echo "\" alt=\"Author 3\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"testimonial-bubble\">
\t\t\t\t\t\t\t<blockquote>
\t\t\t\t\t\t\t\t<p class=\"quote\">
\t\t                            \"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\"
                        \t\t</p>
                        \t\t<cite class=\"author-info\">
                        \t\t\t- Name Surname,<br>Managing Director at <a href=\"#\">Some Company</a>
                        \t\t</cite>
                        \t</blockquote>
                        \t<div class=\"sprite arrow-speech-bubble\"></div>
                        </div>
                    </div>
\t\t\t\t</div>
\t\t\t</div>
\t    </div>
\t    <!-- End Testimonials -->

         
         ";
    }

    // line 195
    public function block_javascripts($context, array $blocks = array())
    {
        // line 196
        echo "
        
        
  
        
        
        ";
    }

    public function getTemplateName()
    {
        return "crowdBundle:Default/page:acceuil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 196,  256 => 195,  231 => 173,  212 => 157,  190 => 138,  153 => 104,  142 => 96,  131 => 88,  83 => 43,  70 => 33,  57 => 23,  42 => 10,  39 => 9,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* {% block css %}*/
/* */
/*   */
/*         {% endblock%}*/
/*         */
/*         */
/*         */
/*          {% block body %}*/
/*              */
/*              */
/*                      <!-- Homepage Slider -->*/
/*         <div class="homepage-slider">*/
/*         	<div id="sequence">*/
/* 				<ul class="sequence-canvas">*/
/* 					<!-- Slide 1 -->*/
/* 					<li class="bg4">*/
/* 						<!-- Slide Title -->*/
/* 						<h2 class="title">Responsive</h2>*/
/* 						<!-- Slide Text -->*/
/* 						<h3 class="subtitle">It looks great on desktops, laptops, tablets and smartphones</h3>*/
/* 						<!-- Slide Image -->*/
/* 						<img class="slide-img" src="{{asset('img/homepage-slider/slide1.png')}}" alt="Slide 1" />*/
/* 					</li>*/
/* 					<!-- End Slide 1 -->*/
/* 					<!-- Slide 2 -->*/
/* 					<li class="bg3">*/
/* 						<!-- Slide Title -->*/
/* 						<h2 class="title">Color Schemes</h2>*/
/* 						<!-- Slide Text -->*/
/* 						<h3 class="subtitle">Comes with 5 color schemes and it's easy to make your own!</h3>*/
/* 						<!-- Slide Image -->*/
/* 						<img class="slide-img" src="{{asset('img/homepage-slider/slide2.png')}}" alt="Slide 2" />*/
/* 					</li>*/
/* 					<!-- End Slide 2 -->*/
/* 					<!-- Slide 3 -->*/
/* 					<li class="bg1">*/
/* 						<!-- Slide Title -->*/
/* 						<h2 class="title">Feature Rich</h2>*/
/* 						<!-- Slide Text -->*/
/* 						<h3 class="subtitle">Huge amount of components and over 30 sample pages!</h3>*/
/* 						<!-- Slide Image -->*/
/* 						<img class="slide-img" src="{{asset('img/homepage-slider/slide3.png')}}" alt="Slide 3" />*/
/* 					</li>*/
/* 					<!-- End Slide 3 -->*/
/* 				</ul>*/
/* 				<div class="sequence-pagination-wrapper">*/
/* 					<ul class="sequence-pagination">*/
/* 						<li>1</li>*/
/* 						<li>2</li>*/
/* 						<li>3</li>*/
/* 					</ul>*/
/* 				</div>*/
/* 			</div>*/
/*         </div>*/
/*         <!-- End Homepage Slider -->*/
/* */
/* 		<!-- Press Coverage -->*/
/*         <div class="section">*/
/* 	    	<div class="container">*/
/* 				<div class="row">*/
/* 					<div class="col-md-4 col-sm-6">*/
/* 						<div class="in-press press-wired">*/
/* 							<a href="#">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="col-md-4 col-sm-6">*/
/* 						<div class="in-press press-mashable">*/
/* 							<a href="#">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="col-md-4 col-sm-6">*/
/* 						<div class="in-press press-techcrunch">*/
/* 							<a href="#">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 		<!-- Press Coverage -->*/
/* */
/* 		<!-- Services -->*/
/*         <div class="section">*/
/* 	        <div class="container">*/
/* 	        	<div class="row">*/
/* 	        		<div class="col-md-4 col-sm-6">*/
/* 	        			<div class="service-wrapper">*/
/* 		        			<img src="{{asset('img/service-icon/diamond.png')}}" alt="Service 1">*/
/* 		        			<h3>Aliquam in adipiscing</h3>*/
/* 		        			<p>Praesent rhoncus mauris ac sollicitudin vehicula. Nam fringilla turpis turpis, at posuere turpis aliquet sit amet condimentum</p>*/
/* 		        			<a href="#" class="btn">Read more</a>*/
/* 		        		</div>*/
/* 	        		</div>*/
/* 	        		<div class="col-md-4 col-sm-6">*/
/* 	        			<div class="service-wrapper">*/
/* 		        			<img src="{{asset('img/service-icon/ruler.png')}}" alt="Service 2">*/
/* 		        			<h3>Curabitur mollis</h3>*/
/* 		        			<p>Suspendisse eget libero mi. Fusce ligula orci, vulputate nec elit ultrices, ornare faucibus orci. Aenean lectus sapien, vehicula</p>*/
/* 		        			<a href="#" class="btn">Read more</a>*/
/* 		        		</div>*/
/* 	        		</div>*/
/* 	        		<div class="col-md-4 col-sm-6">*/
/* 	        			<div class="service-wrapper">*/
/* 		        			<img src="{{asset('img/service-icon/box.png')}}" alt="Service 3">*/
/* 		        			<h3>Vivamus mattis</h3>*/
/* 		        			<p>Phasellus posuere et nisl ac commodo. Nulla facilisi. Sed tincidunt bibendum cursus. Aenean vulputate aliquam risus rutrum scelerisque</p>*/
/* 		        			<a href="#" class="btn">Read more</a>*/
/* 		        		</div>*/
/* 	        		</div>*/
/* 	        	</div>*/
/* 	        </div>*/
/* 	    </div>*/
/* 	    <!-- End Services -->*/
/* */
/* 		<!-- Call to Action Bar -->*/
/* 	    <div class="section section-white">*/
/* 			<div class="container">*/
/* 				<div class="row">*/
/* 					<div class="col-md-12">*/
/* 						<div class="calltoaction-wrapper">*/
/* 							<h3>It's a free multipurpose Bootstrap 3 template!</h3> <a href="http://www.dragdropsite.com" class="btn btn-orange">Download here!</a>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 		<!-- End Call to Action Bar -->*/
/* */
/* 		<!-- Testimonials -->*/
/* 	    <div class="section">*/
/* 			<div class="container">*/
/* 				<h2>Testimonials</h2>*/
/* 				<div class="row">*/
/* 					<!-- Testimonial -->*/
/* 					<div class="testimonial col-md-4 col-sm-6">*/
/* 						<!-- Author Photo -->*/
/* 						<div class="author-photo">*/
/* 							<img src="{{asset('img/user1.jpg')}}" alt="Author 1">*/
/* 						</div>*/
/* 						<div class="testimonial-bubble">*/
/* 							<blockquote>*/
/* 								<!-- Quote -->*/
/* 								<p class="quote">*/
/* 		                            "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut."*/
/*                         		</p>*/
/*                         		<!-- Author Info -->*/
/*                         		<cite class="author-info">*/
/*                         			- Name Surname,<br>Managing Director at <a href="#">Some Company</a>*/
/*                         		</cite>*/
/*                         	</blockquote>*/
/*                         	<div class="sprite arrow-speech-bubble"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- End Testimonial -->*/
/*                     <div class="testimonial col-md-4 col-sm-6">*/
/* 						<div class="author-photo">*/
/* 							<img src="{{asset('img/user5.jpg')}}" alt="Author 2">*/
/* 						</div>*/
/* 						<div class="testimonial-bubble">*/
/* 							<blockquote>*/
/* 								<p class="quote">*/
/* 		                            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo."*/
/*                         		</p>*/
/*                         		<cite class="author-info">*/
/*                         			- Name Surname,<br>Managing Director at <a href="#">Some Company</a>*/
/*                         		</cite>*/
/*                         	</blockquote>*/
/*                         	<div class="sprite arrow-speech-bubble"></div>*/
/*                         </div>*/
/*                     </div>*/
/* 					<div class="testimonial col-md-4 col-sm-6">*/
/* 						<div class="author-photo">*/
/* 							<img src="{{asset('img/user2.jpg')}}" alt="Author 3">*/
/* 						</div>*/
/* 						<div class="testimonial-bubble">*/
/* 							<blockquote>*/
/* 								<p class="quote">*/
/* 		                            "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."*/
/*                         		</p>*/
/*                         		<cite class="author-info">*/
/*                         			- Name Surname,<br>Managing Director at <a href="#">Some Company</a>*/
/*                         		</cite>*/
/*                         	</blockquote>*/
/*                         	<div class="sprite arrow-speech-bubble"></div>*/
/*                         </div>*/
/*                     </div>*/
/* 				</div>*/
/* 			</div>*/
/* 	    </div>*/
/* 	    <!-- End Testimonials -->*/
/* */
/*          */
/*          {% endblock %}*/
/*          */
/*         {% block javascripts %}*/
/* */
/*         */
/*         */
/*   */
/*         */
/*         */
/*         {% endblock %}*/
