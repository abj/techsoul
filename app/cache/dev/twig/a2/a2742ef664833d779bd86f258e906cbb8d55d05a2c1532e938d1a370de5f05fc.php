<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_2b5841bc9206aa10588b31aaff4c757b6731cacce47549fee17a80557d97040b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "FOSUserBundle:Resetting:request_content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "


    <!-- Page Title -->
    <div class=\"section section-breadcrumbs\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <h1>Password Reset</h1>
                </div>
            </div>
        </div>
    </div>

    <div class=\"section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <div class=\"basic-login\">
                        ";
        // line 25
        echo "
                        <form action=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request\">
                            <div>
                                ";
        // line 28
        if (array_key_exists("invalid_username", $context)) {
            // line 29
            echo "                                    <p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.invalid_username", array("%username%" => (isset($context["invalid_username"]) ? $context["invalid_username"] : $this->getContext($context, "invalid_username"))), "FOSUserBundle"), "html", null, true);
            echo "</p>
                                ";
        }
        // line 31
        echo "
                                <div class=\"form-group\">
                                    <label for=\"username\"><i class=\"icon-envelope\"></i> <b>";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</b></label>
                                    <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
                                </div>

                                <div class=\"form-group\">

                                    <button type=\"submit\" class=\"btn pull-right\"  value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" >Reset Password</button>
                                    <div class=\"clearfix\"></div>
                                </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>


";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 39,  72 => 33,  68 => 31,  62 => 29,  60 => 28,  55 => 26,  52 => 25,  31 => 5,  28 => 4,  11 => 2,);
    }
}
/* {# mot de passe oublier #}*/
/* {% extends "::base.html.twig" %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/*     <!-- Page Title -->*/
/*     <div class="section section-breadcrumbs">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <h1>Password Reset</h1>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="section">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-sm-6 col-sm-offset-3">*/
/*                     <div class="basic-login">*/
/*                         {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*                         <form action="{{ path('fos_user_resetting_send_email') }}" method="POST" class="fos_user_resetting_request">*/
/*                             <div>*/
/*                                 {% if invalid_username is defined %}*/
/*                                     <p>{{ 'resetting.request.invalid_username'|trans({'%username%': invalid_username}) }}</p>*/
/*                                 {% endif %}*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label for="username"><i class="icon-envelope"></i> <b>{{ 'resetting.request.username'|trans }}</b></label>*/
/*                                     <input type="text" id="username" name="username" required="required" />*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <button type="submit" class="btn pull-right"  value="{{ 'resetting.request.submit'|trans }}" >Reset Password</button>*/
/*                                     <div class="clearfix"></div>*/
/*                                 </div>*/
/*                         </form>*/
/* */
/* */
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* */
/* {% endblock %}*/
