<?php

/* utilisateurBundle:Comment:show.html.twig */
class __TwigTemplate_73bb4e319aa077cc9f03f2c277378dab9cc258338c8e1472672f53fe8c088e4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Comment:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"section section-breadcrumbs\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h1> Commentaire </h1>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
     
    
<div class=\"container\">
 
  <a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("comment_new");
        echo "\">
                Create a new entry
            </a>
  
  
  <table class=\"table table-striped\">
    <thead>
      <tr>
        <th> ID </th>
        <th>Score</th>
        <th>commentaire</th>
        <th>Actions</th>
     
      </tr>
    </thead>
    <tbody>
       
      <tr>
        <td>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
        echo "</td>
      <td>  ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "score", array()), "html", null, true);
        echo "</td>
      <td>  ";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "body", array()), "html", null, true);
        echo "</td>
      
        <td><a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("comment");
        echo "\">
            Back to the list
        </a></td>
        <td>  <a href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("comment_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">
            Edit
        </a></td>
        <td>     ";
        // line 46
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</td>
      </tr>
     
    </tbody>
  </table>
</div>

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Comment:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 46,  86 => 43,  80 => 40,  75 => 38,  71 => 37,  67 => 36,  46 => 18,  31 => 5,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     */
/*     	<div class="section section-breadcrumbs">*/
/* 			<div class="container">*/
/* 				<div class="row">*/
/* 					<div class="col-md-12">*/
/* 						<h1> Commentaire </h1>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*      */
/*     */
/* <div class="container">*/
/*  */
/*   <a href="{{ path('comment_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*   */
/*   */
/*   <table class="table table-striped">*/
/*     <thead>*/
/*       <tr>*/
/*         <th> ID </th>*/
/*         <th>Score</th>*/
/*         <th>commentaire</th>*/
/*         <th>Actions</th>*/
/*      */
/*       </tr>*/
/*     </thead>*/
/*     <tbody>*/
/*        */
/*       <tr>*/
/*         <td>{{ entity.id }}</td>*/
/*       <td>  {{ entity.score }}</td>*/
/*       <td>  {{ entity.body }}</td>*/
/*       */
/*         <td><a href="{{ path('comment') }}">*/
/*             Back to the list*/
/*         </a></td>*/
/*         <td>  <a href="{{ path('comment_edit', { 'id': entity.id }) }}">*/
/*             Edit*/
/*         </a></td>*/
/*         <td>     {{ form(delete_form) }}</td>*/
/*       </tr>*/
/*      */
/*     </tbody>*/
/*   </table>*/
/* </div>*/
/* */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/* {% endblock %}*/
/* */
