<?php

/* :admin:footer.html.twig */
class __TwigTemplate_2e286a6f00ce3fd188654008f781086a2c01cd4ce35acdc7a96e2c24f5e0ad51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " <!-- Footer -->
\t        <div class=\"footer clearfix\">
\t\t        <div class=\"pull-left\">&copy; 2013. Londinium Admin Template by <a href=\"http://themeforest.net/user/Kopyov\">Eugene Kopyov</a></div>
\t        \t<div class=\"pull-right icons-group\">
\t        \t\t<a href=\"#\"><i class=\"icon-screen2\"></i></a>
\t        \t\t<a href=\"#\"><i class=\"icon-balance\"></i></a>
\t        \t\t<a href=\"#\"><i class=\"icon-cog3\"></i></a>
\t        \t</div>
\t        </div>
\t        <!-- /footer -->

";
    }

    public function getTemplateName()
    {
        return ":admin:footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/*  <!-- Footer -->*/
/* 	        <div class="footer clearfix">*/
/* 		        <div class="pull-left">&copy; 2013. Londinium Admin Template by <a href="http://themeforest.net/user/Kopyov">Eugene Kopyov</a></div>*/
/* 	        	<div class="pull-right icons-group">*/
/* 	        		<a href="#"><i class="icon-screen2"></i></a>*/
/* 	        		<a href="#"><i class="icon-balance"></i></a>*/
/* 	        		<a href="#"><i class="icon-cog3"></i></a>*/
/* 	        	</div>*/
/* 	        </div>*/
/* 	        <!-- /footer -->*/
/* */
/* */
