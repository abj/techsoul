<?php

/* crowdBundle:Default/acceuil:acceuil.html.twig */
class __TwigTemplate_76b8bdf292015a944057890981e072a3365eac5f79a20f81f365576bf70cd6ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "crowdBundle:Default/acceuil:acceuil.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    
    

            <!-- Start of Header Banner -->             
            <section class=\"banner_slider mbtm\"> 
                <ul id=\"banner_slider\"> 
                    <li> 
                        <img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_1.jpg"), "html", null, true);
        echo "\" alt=\"Banner Slider\" /> 
                        <div class=\"slider_content\"> 
                            <p class=\"b_dark\"> we need your support</p> 
                            <span class=\"clear\"></span> 
                            <p class=\"b_green\"> to educate, feed and accomodate!</p> 
                        </div>                         
                    </li>                     
                    <li> 
                        <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" alt=\"Banner Slider\" /> 
                        <div class=\"slider_content\"> 
                            <p class=\"b_dark\"> we need your support</p> 
                            <span class=\"clear\"></span> 
                            <p class=\"b_green\"> to educate, feed and accomodate!</p> 
                        </div>                         
                    </li>                     
                    <li> 
                        <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" alt=\"Banner Slider\" /> 
                        <div class=\"slider_content\"> 
                            <p class=\"b_dark\"> we need your support</p> 
                            <span class=\"clear\"></span> 
                            <p class=\"b_green\"> to educate, feed and accomodate!</p> 
                        </div>                         
                    </li>                     
                    <li> 
                        <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_4.jpg"), "html", null, true);
        echo "\" alt=\"Banner Slider\" /> 
                        <div class=\"slider_content\"> 
                            <p class=\"b_dark\"> we need your support</p> 
                            <span class=\"clear\"></span> 
                            <p class=\"b_green\"> to educate, feed and accomodate!</p> 
                        </div>                         
                    </li>                     
                    <li> 
                        <img src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_5.jpg"), "html", null, true);
        echo "\" alt=\"Banner Slider\" /> 
                        <div class=\"slider_content\"> 
                            <p class=\"b_dark\"> we need your support</p> 
                            <span class=\"clear\"></span> 
                            <p class=\"b_green\"> to educate, feed and accomodate!</p> 
                        </div>                         
                    </li>                     
                </ul>                 
            </section>             
            <!-- End of Header Banner -->             
            <!-- Start of Features Boxes -->             
            <section id=\"ngo_features\" class=\"mbtm\"> 
                <section class=\"container-fluid container\"> 
                    <section class=\"row-fluid\"> 
                        <!-- Start of Features Box 1 -->                         
                        <figure class=\"span3 feature\"> 
                            <div class=\"ftr_img f-img-1\"> 
                                <span class=\"img\"> River Floods </span> 
                            </div>                             
                            <div class=\"ftr_txt\"> 
                                <strong>Poster des idees</strong> 
                                <p> River floods prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> 
                            </div>                             
                        </figure>                         
                        <!-- End of Features Boxes 1 -->                         
                        <!-- Start of Features Box 2 -->                         
                        <figure class=\"span3 feature\"> 
                            <div class=\"ftr_img f-img-2\"> 
                                <span class=\"img\"> Volcanic Eruptions </span> 
                            </div>                             
                            <div class=\"ftr_txt\"> 
                                <strong> Financer Des projets</strong> 
                                <p> Volcano alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> 
                            </div>                             
                        </figure>                         
                        <!-- End of Features Boxes 2 -->                         
                        <!-- Start of Features Box 3 -->                         
                        <figure class=\"span3 feature\"> 
                            <div class=\"ftr_img f-img-3\"> 
                                <span class=\"img\"> Thunder Storms  </span> 
                            </div>                             
                            <div class=\"ftr_txt\"> 
                                <strong>Trouver des Competences</strong> 
                                <p> Malorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> 
                            </div>                             
                        </figure>                         
                        <!-- End of Features Boxes 3 -->                         
                        <!-- Start of Features Box 4 -->                         
                        <figure class=\"span3 feature\"> 
                            <div class=\"ftr_img f-img-4\"> 
                                <span class=\"img\"> Tsunami </span> 
                            </div>                             
                            <div class=\"ftr_txt\"> 
                                <strong> Participer a des evenements</strong> 
                                <p> An malorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> 
                            </div>                             
                        </figure>                         
                        <!-- End of Features Boxes 4 -->                         
                    </section>                     
                </section>                 
            </section>             
            <!-- End of Features Boxes -->             
            <!-- Start of Donation Box -->             
            <section id=\"donation_box\" class=\"mbtm\"> 
                <section class=\"container container-fluid\"> 
                    <section class=\"row-fluid\"> 
                        <figure class=\"span10\"> 
                            <h2>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;aider des projets &nbsp;<strong>innovant&nbsp;</strong> pour qu'il &nbsp;<strong>voit le jour !</strong> </h2> 
                        </figure>                         
                        <figure class=\"span2\"> 
                            <form action=\"#\"> 
                                <input type=\"hidden\" value=\"5.00\" name=\"amout\" /> 
                                <input type=\"hidden\" value=\"USD\" name=\"Currency\" /> 
                                <button data-placement=\"top\" rel=\"tooltip\" title=\"Support Us\" class=\" btn btn-large dropdown-toggle\" type=\"submit\">Donate Now</button>                                 
                            </form>                             
                        </figure>                         
                    </section>                     
                </section>                 
            </section>             
            <!-- End of Donation Box -->             
            <!-- Start of Charity News & Progress Section -->             
            <section id=\"progress_news\" class=\"mbtm\"> 
                <section class=\"container-fluid container\"> 
                    <section class=\"span12 first projects_holder\"> 
                        <section class=\"span4 first fund_project\" id=\"charity_progress\"> 
                            <h3> <a href=\"crowdfunding_detail.html\"> Help us build our hospital </a> </h3> 
                            <img src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/crowd_funding_1.jpg"), "html", null, true);
        echo "\" alt=\"Fund Featured Image\" /> 
                            <span class=\"current_collection\">  \$18,387 </span> 
                            <h4> Pledged of \$200,000 Goal </h4> 
                            <div class=\"progress progress-striped active\"> 
                                <div class=\"bar p80\"></div>                                 
                            </div>                             
                            <div class=\"info\"> 
                                <div class=\"span6 first\"> 
                                    <i class=\"icon-user\"></i> 
                                    <span> 147 </span> Pledgers
                                </div>                                 
                                <div class=\"span6\"> 
                                    <i class=\"icon-calendar-empty\"></i> 
                                    <span> 204 </span> Days Left
                                </div>                                 
                            </div>                             
                        </section>                         
                        <section class=\"span4 fund_project\" id=\"charity_progress\"> 
                            <h3> <a href=\"crowdfunding_detail.html\"> Help us educate children </a> </h3> 
                            <img src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/crowd_funding_2.jpg"), "html", null, true);
        echo "\" alt=\"Fund Featured Image\" /> 
                            <span class=\"current_collection\"> \$12,265 </span> 
                            <h4> Pledged of \$200,000 Goal </h4> 
                            <div class=\"progress progress-striped active\"> 
                                <div class=\"bar p80\"></div>                                 
                            </div>                             
                            <div class=\"info\"> 
                                <div class=\"span6 first\"> 
                                    <i class=\"icon-user\"></i> 
                                    <span> 147 </span> Pledgers
                                </div>                                 
                                <div class=\"span6\"> 
                                    <i class=\"icon-calendar-empty\"></i> 
                                    <span> 204 </span> Days Left
                                </div>                                 
                            </div>                             
                        </section>                         
                        <section class=\"span4 fund_project\" id=\"charity_progress\"> 
                            <h3> <a href=\"crowdfunding_detail.html\"> Help us for haiti victams </a> </h3> 
                            <img src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/crowd_funding_3.jpg"), "html", null, true);
        echo "\" alt=\"Fund Featured Image\" /> 
                            <span class=\"current_collection\">  \$9,721 </span> 
                            <h4> Pledged of \$200,000 Goal </h4> 
                            <div class=\"progress progress-striped active\"> 
                                <div class=\"bar p80\"></div>                                 
                            </div>                             
                            <div class=\"info\"> 
                                <div class=\"span6 first\"> 
                                    <i class=\"icon-user\"></i> 
                                    <span> 39 </span> Pledgers
                                </div>                                 
                                <div class=\"span6\"> 
                                    <i class=\"icon-calendar-empty\"></i> 
                                    <span> 74 </span> Days Left
                                </div>                                 
                            </div>                             
                        </section>                         
                        <hr /> 
                        <hr /> 
                    </section>                     
                </section>                 
            </section>             
            <!-- End of Charity News & Progress Section -->             
            <!-- Start of Event & Videos -->             
            <section id=\"events_videos\" class=\"mbtm\"> 
                <section class=\"container-fluid container\"> 
                    <section class=\"row-fluid\">                          
                        <figure class=\"span6 first\" id=\"video_slider\"> 
                            <h2> NOS <span> Evenement a venir&nbsp;</span> </h2> 
                            <div class=\"video_slider_container span8 offset2\"> 
                                <ul class=\"video_slider\"> 
                                    <li> 
                                        <div class=\"video\"> 
                                            <iframe src=\"http://player.vimeo.com/video/1823335?title=0&amp;byline=0&amp;portrait=0\" width=\"376\" height=\"230\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>                                             
                                            <div class=\"tag_line\"> Food & Shelter provided to Katrina Victams </div>                                             
                                        </div>                                         
                                    </li>                                     
                                </ul>                                 
                            </div>                             
                        </figure>
                        <figure class=\"span5\" id=\"news_accordion\"> 
                            <div class=\"accordion\" id=\"accordion_news\"> 
                                <div class=\"accordion-group\"> 
                                    <div class=\"accordion-heading\"> 
                                        <a class=\"accordion-toggle active\" data-toggle=\"collapse\" data-parent=\"#accordion_news\" href=\"#co\"> <span class=\"datem span3 first\"> 05/22 </span> <span class=\"title span8\"> City Marathon 2013 
<span class=\"location_date\"> <span class=\"location\"> <i class=\"icon-map-marker\"></i> London </span> <span class=\"date\"> <i class=\"icon-time\"></i>\t\tMay22, 2013\t </span> </span> </span> <span class=\"span1\" id=\"icon_toggle\"> <i class=\"icon-minus\"> </i></span> </a> 
                                    </div>                                     
                                    <div id=\"co\" class=\"accordion-body collapse in\"> 
                                        <div class=\"accordion-inner\"> 
                                            <figure class=\"span3 img first\"> 
                                                <img src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/accordion_img.jpg"), "html", null, true);
        echo "\" alt=\"\" /> 
                                            </figure>                                             
                                            <figure class=\"span9\"> 
                                                <p> City Marathon 2013 alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> 
                                                <a href=\"#\"> Read More</a> 
                                            </figure>                                             
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                                <div class=\"accordion-group\"> 
                                    <div class=\"accordion-heading\"> 
                                        <a class=\"accordion-toggle inactive\" data-toggle=\"collapse\" data-parent=\"#accordion_news\" href=\"#c2\"> <span class=\"datem span3 first\"> 05/27 </span> <span class=\"title span8\"> Kids Fun Gala 
<span class=\"location_date\"> <span class=\"location\"> <i class=\"icon-map-marker\"></i> London </span> <span class=\"date\"> <i class=\"icon-time\"></i>\t\tMay 27, 2013\t </span> </span> </span> <span class=\"span1\" id=\"icon_toggle\"> <i class=\"icon-plus\"> </i></span> </a> 
                                    </div>                                     
                                    <div id=\"c2\" class=\"accordion-body collapse\"> 
                                        <div class=\"accordion-inner\"> 
                                            <figure class=\"span3 img first\"> 
                                                <img src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/accordion_img.jpg"), "html", null, true);
        echo "\" alt=\"\" /> 
                                            </figure>                                             
                                            <figure class=\"span9\"> 
                                                <p> City Marathon 2013 alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> 
                                                <a href=\"#\"> Read More</a> 
                                            </figure>                                             
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                                <div class=\"accordion-group\"> 
                                    <div class=\"accordion-heading\"> 
                                        <a class=\"accordion-toggle inactive\" data-toggle=\"collapse\" data-parent=\"#accordion_news\" href=\"#c3\"> <span class=\"datem span3 first\"> 05/29 </span> <span class=\"title span8\"> Family Festival 
<span class=\"location_date\"> <span class=\"location\"> <i class=\"icon-map-marker\"></i> London </span> <span class=\"date\"> <i class=\"icon-time\"></i>\t\tMay 29, 2013\t </span> </span> </span> <span class=\"span1\" id=\"icon_toggle\"> <i class=\"icon-plus\"> </i></span> </a> 
                                    </div>                                     
                                    <div id=\"c3\" class=\"accordion-body collapse\"> 
                                        <div class=\"accordion-inner\"> 
                                            <figure class=\"span3 img first\"> 
                                                <img src=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/accordion_img.jpg"), "html", null, true);
        echo "\" alt=\"\" /> 
                                            </figure>                                             
                                            <figure class=\"span9\"> 
                                                <p> City Marathon 2013 alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> 
                                                <a href=\"#\"> Read More</a> 
                                            </figure>                                             
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>                             
                        </figure>                         
                    </section>                     
                </section>                 
            </section>             
            <!-- End Of Event & Videos -->             
            <!-- Start of Blog & Store -->             
            <section id=\"blog_store\" class=\"mbtm\"> 
                <section class=\"container-fluid container\"> 
</section>                 
            </section>             
            <!-- End of Blog & Store -->             
            <!-- Start of Footer -->             
            <footer id=\"footer\" class=\"mtp\"> 
                <!-- Start of Footer 1 -->                 
                <section class=\"footer_1\"> 
                    <section class=\"container-fluid container\"> 
                        <section class=\"row-fluid\"> 
                            <section id=\"banner\" class=\"span12 first\"> 
                                <div class=\"inner\"> 
                                    <div class=\"span9\"> 
                                        <h2> Free shipping on all orders over \$75 </h2> 
                                        <h3> * FREE OVER \$125 for international orders </h3> 
                                    </div>                                     
                                    <div class=\"pull-left span3\"> 
</div>                                     
                                </div>                                 
                            </section>                             
                            <section class=\"span12 first\" id=\"footer_main\"> 
                                <section class=\"span3 first widget\"> 
                                    <h4>GET IN <span> Touch </span> <span class=\"h-line\"></span> </h4> 
                                    <form id=\"contact_form\" method=\"post\" action=\"contact.php\"> 
                                        <input type=\"text\" value=\"Name\" name=\"name\" required /> 
                                        <input type=\"email\" value=\"Email\" name=\"email\" required /> 
                                        <textarea rows=\"5\" cols=\"20\" name=\"comments\" required>Comments</textarea>                                         
                                        <input type=\"submit\" name=\"submit\" value=\"Send Message\" /> 
                                    </form>                                     
                                </section>                                 
                                <section class=\"span3 widget popular_post\"> 
                                    <h4>Blog Popular <span> Post </span> <span class=\"h-line\"></span> </h4> 
                                    <ul id=\"popular_post\"> 
                                        <li> 
                                            <span> <i class=\"icon-facetime-video\"></i> </span> 
                                            <p> Assum mucius maiestatis et nam purto virtute\t </p>
                                        </li>                                         
                                        <li> 
                                            <span> <i class=\"icon-volume-up\"></i></span> 
                                            <p> <a href=\"#\"> Assum mucius maiestatis et nam purto virtute\t</a> </p>
                                        </li>                                         
                                        <li> 
                                            <span> <i class=\"icon-picture\"></i> </span> 
                                            <p> Assum mucius maiestatis et nam purto virtute\t </p>
                                        </li>                                         
                                        <li> 
                                            <span> <i class=\"icon-volume-up\"></i> </span> 
                                            <p> Assum mucius maiestatis et nam purto virtute\t </p>
                                        </li>                                         
                                    </ul>                                     
                                    <a class=\"v-a\" href=\"http://themeforest/user/crunchpress/\">+ View All <span class=\"h-line\"></span></a> 
                                </section>                                 
                                <section class=\"widget span3\"> 
                                    <h4>Latest Twitter <span> Updates </span> <span class=\"h-line\"></span> </h4> 
                                    <ul id=\"tweets_crunchpress\"> 
                                        <li> New Theme Called Fine Food ready to rocks! Check this out! 
                                            <span> <a href=\"#\"> http://fb.me/90DF91 </a> </span> 
                                            <span> - 2 hour ago </span> 
                                        </li>                                         
                                        <li> The Church Issues Fixed. I hope update will be ready  soon. Then you guys download it. Before upload new  
                                            <span> <a href=\"#\">http://fb.me/4rtIOlp9  </a> </span> 
                                            <span> -1 Day ago </span> 
                                        </li>                                         
                                    </ul>                                     
                                    <a class=\"v-a\" href=\"http://themeforest/user/crunchpress/\">+ View All <span class=\"h-line\"></span></a> 
                                </section>                                 
                                <section class=\"span3 widget\"> 
                                    <h4>LATEST EVENTS <span> GALLERY </span> <span class=\"h-line\"></span> </h4> 
                                    <ul class=\"gallery-list gallery_widget\"> 
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 338
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_1.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 340
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 344
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_2.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 346
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_5.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 350
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_3.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 352
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 356
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_4.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_4.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 362
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_3.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 364
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 368
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_4.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 370
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_4.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 374
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_2.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 376
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_5.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 380
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_1.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 382
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 386
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_2.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 388
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_5.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 392
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_1.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 394
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_3.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 398
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_3.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 400
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_2.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                        <li class=\"view_new view-tenth\"> 
                                            <img class=\"galler-img\" src=\"";
        // line 404
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gallery_img_4.jpg"), "html", null, true);
        echo "\" alt=\"\"> 
                                            <div class=\"mask\"> 
                                                <a class=\"image-gal info\" rel=\"prettyPhoto[gallery1]\" href=\"";
        // line 406
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/t_slider_4.jpg"), "html", null, true);
        echo "\" title=\"\">&nbsp;</a> 
                                            </div>                                             
                                        </li>                                         
                                    </ul>                                     
                                    <a class=\"v-a\" href=\"http://themeforest/user/crunchpress/\">+ View All <span class=\"h-line\"></span></a> 
                                </section>                                 
                            </section>                             
                        </section>                         
                    </section>                     
                </section>                 
                <!-- End of Footer 1 -->                 
                <!-- Start of Footer 2 -->                 
                <section class=\"footer_2\"> 
                    <section class=\"container-fluid container\"> 
                        <section class=\"row-fluid\"> 
                            <figure class=\"span6\" id=\"footer_left\"> 
                                <i class=\"icon-mobile-phone\"></i> 
                                <span> +00 71 900 34 45 </span> 
                                <i class=\"icon-envelope-alt\"></i> 
                                <span> contact@companyname.com  </span> 
                            </figure>                             
                            <figure class=\"span6\" id=\"footer_right\"> 
                                <div id=\"socialicons\"> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Join us on Facebook\" id=\"social_facebook\" class=\"social_active\" href=\"#\"> <span> </span></a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Follow us on Twitter\" id=\"social_twitter\" class=\"social_active\" href=\"#\"> <span> </span></a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Visit LinkedIn Page\" id=\"social_linkedin\" class=\"social_active\" href=\"#\"> <span> </span></a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Browse Flicker Gallery\" id=\"social_flickr\" class=\"social_active\" href=\"#\"> <span> </span></a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Check us on Forst \" id=\"social_forst\" class=\"social_active\" href=\"#\"> <span> </span></a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"View Viemeo Video Collection\" id=\"social_vimeo\" class=\"social_active\"> <span> </span></a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Pin Us\" id=\"social_pinterest\" class=\"social_active\" href=\"#\"> <span></span> </a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Join our Social Circle\" id=\"social_google_plus\" class=\"social_active\"> <span> </span></a> 
                                    <a data-placement=\"top\" rel=\"tooltip\" title=\"Watch Our Broadcasting\" id=\"social_youtube\" class=\"social_active\" href=\"#\"> <span> </span></a> 
                                </div>                                 
                            </figure>                             
                        </section>                         
                    </section>                     
                </section>   

";
    }

    public function getTemplateName()
    {
        return "crowdBundle:Default/acceuil:acceuil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  537 => 406,  532 => 404,  525 => 400,  520 => 398,  513 => 394,  508 => 392,  501 => 388,  496 => 386,  489 => 382,  484 => 380,  477 => 376,  472 => 374,  465 => 370,  460 => 368,  453 => 364,  448 => 362,  441 => 358,  436 => 356,  429 => 352,  424 => 350,  417 => 346,  412 => 344,  405 => 340,  400 => 338,  310 => 251,  290 => 234,  270 => 217,  217 => 167,  195 => 148,  173 => 129,  84 => 43,  73 => 35,  62 => 27,  51 => 19,  40 => 11,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* */
/* {% block body %}*/
/*     */
/*     */
/* */
/*             <!-- Start of Header Banner -->             */
/*             <section class="banner_slider mbtm"> */
/*                 <ul id="banner_slider"> */
/*                     <li> */
/*                         <img src="{{asset('images/t_slider_1.jpg')}}" alt="Banner Slider" /> */
/*                         <div class="slider_content"> */
/*                             <p class="b_dark"> we need your support</p> */
/*                             <span class="clear"></span> */
/*                             <p class="b_green"> to educate, feed and accomodate!</p> */
/*                         </div>                         */
/*                     </li>                     */
/*                     <li> */
/*                         <img src="{{asset('images/t_slider_2.jpg')}}" alt="Banner Slider" /> */
/*                         <div class="slider_content"> */
/*                             <p class="b_dark"> we need your support</p> */
/*                             <span class="clear"></span> */
/*                             <p class="b_green"> to educate, feed and accomodate!</p> */
/*                         </div>                         */
/*                     </li>                     */
/*                     <li> */
/*                         <img src="{{asset('images/t_slider_3.jpg')}}" alt="Banner Slider" /> */
/*                         <div class="slider_content"> */
/*                             <p class="b_dark"> we need your support</p> */
/*                             <span class="clear"></span> */
/*                             <p class="b_green"> to educate, feed and accomodate!</p> */
/*                         </div>                         */
/*                     </li>                     */
/*                     <li> */
/*                         <img src="{{asset('images/t_slider_4.jpg')}}" alt="Banner Slider" /> */
/*                         <div class="slider_content"> */
/*                             <p class="b_dark"> we need your support</p> */
/*                             <span class="clear"></span> */
/*                             <p class="b_green"> to educate, feed and accomodate!</p> */
/*                         </div>                         */
/*                     </li>                     */
/*                     <li> */
/*                         <img src="{{asset('images/t_slider_5.jpg')}}" alt="Banner Slider" /> */
/*                         <div class="slider_content"> */
/*                             <p class="b_dark"> we need your support</p> */
/*                             <span class="clear"></span> */
/*                             <p class="b_green"> to educate, feed and accomodate!</p> */
/*                         </div>                         */
/*                     </li>                     */
/*                 </ul>                 */
/*             </section>             */
/*             <!-- End of Header Banner -->             */
/*             <!-- Start of Features Boxes -->             */
/*             <section id="ngo_features" class="mbtm"> */
/*                 <section class="container-fluid container"> */
/*                     <section class="row-fluid"> */
/*                         <!-- Start of Features Box 1 -->                         */
/*                         <figure class="span3 feature"> */
/*                             <div class="ftr_img f-img-1"> */
/*                                 <span class="img"> River Floods </span> */
/*                             </div>                             */
/*                             <div class="ftr_txt"> */
/*                                 <strong>Poster des idees</strong> */
/*                                 <p> River floods prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> */
/*                             </div>                             */
/*                         </figure>                         */
/*                         <!-- End of Features Boxes 1 -->                         */
/*                         <!-- Start of Features Box 2 -->                         */
/*                         <figure class="span3 feature"> */
/*                             <div class="ftr_img f-img-2"> */
/*                                 <span class="img"> Volcanic Eruptions </span> */
/*                             </div>                             */
/*                             <div class="ftr_txt"> */
/*                                 <strong> Financer Des projets</strong> */
/*                                 <p> Volcano alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> */
/*                             </div>                             */
/*                         </figure>                         */
/*                         <!-- End of Features Boxes 2 -->                         */
/*                         <!-- Start of Features Box 3 -->                         */
/*                         <figure class="span3 feature"> */
/*                             <div class="ftr_img f-img-3"> */
/*                                 <span class="img"> Thunder Storms  </span> */
/*                             </div>                             */
/*                             <div class="ftr_txt"> */
/*                                 <strong>Trouver des Competences</strong> */
/*                                 <p> Malorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> */
/*                             </div>                             */
/*                         </figure>                         */
/*                         <!-- End of Features Boxes 3 -->                         */
/*                         <!-- Start of Features Box 4 -->                         */
/*                         <figure class="span3 feature"> */
/*                             <div class="ftr_img f-img-4"> */
/*                                 <span class="img"> Tsunami </span> */
/*                             </div>                             */
/*                             <div class="ftr_txt"> */
/*                                 <strong> Participer a des evenements</strong> */
/*                                 <p> An malorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> */
/*                             </div>                             */
/*                         </figure>                         */
/*                         <!-- End of Features Boxes 4 -->                         */
/*                     </section>                     */
/*                 </section>                 */
/*             </section>             */
/*             <!-- End of Features Boxes -->             */
/*             <!-- Start of Donation Box -->             */
/*             <section id="donation_box" class="mbtm"> */
/*                 <section class="container container-fluid"> */
/*                     <section class="row-fluid"> */
/*                         <figure class="span10"> */
/*                             <h2>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;aider des projets &nbsp;<strong>innovant&nbsp;</strong> pour qu'il &nbsp;<strong>voit le jour !</strong> </h2> */
/*                         </figure>                         */
/*                         <figure class="span2"> */
/*                             <form action="#"> */
/*                                 <input type="hidden" value="5.00" name="amout" /> */
/*                                 <input type="hidden" value="USD" name="Currency" /> */
/*                                 <button data-placement="top" rel="tooltip" title="Support Us" class=" btn btn-large dropdown-toggle" type="submit">Donate Now</button>                                 */
/*                             </form>                             */
/*                         </figure>                         */
/*                     </section>                     */
/*                 </section>                 */
/*             </section>             */
/*             <!-- End of Donation Box -->             */
/*             <!-- Start of Charity News & Progress Section -->             */
/*             <section id="progress_news" class="mbtm"> */
/*                 <section class="container-fluid container"> */
/*                     <section class="span12 first projects_holder"> */
/*                         <section class="span4 first fund_project" id="charity_progress"> */
/*                             <h3> <a href="crowdfunding_detail.html"> Help us build our hospital </a> </h3> */
/*                             <img src="{{asset('images/crowd_funding_1.jpg')}}" alt="Fund Featured Image" /> */
/*                             <span class="current_collection">  $18,387 </span> */
/*                             <h4> Pledged of $200,000 Goal </h4> */
/*                             <div class="progress progress-striped active"> */
/*                                 <div class="bar p80"></div>                                 */
/*                             </div>                             */
/*                             <div class="info"> */
/*                                 <div class="span6 first"> */
/*                                     <i class="icon-user"></i> */
/*                                     <span> 147 </span> Pledgers*/
/*                                 </div>                                 */
/*                                 <div class="span6"> */
/*                                     <i class="icon-calendar-empty"></i> */
/*                                     <span> 204 </span> Days Left*/
/*                                 </div>                                 */
/*                             </div>                             */
/*                         </section>                         */
/*                         <section class="span4 fund_project" id="charity_progress"> */
/*                             <h3> <a href="crowdfunding_detail.html"> Help us educate children </a> </h3> */
/*                             <img src="{{asset('images/crowd_funding_2.jpg')}}" alt="Fund Featured Image" /> */
/*                             <span class="current_collection"> $12,265 </span> */
/*                             <h4> Pledged of $200,000 Goal </h4> */
/*                             <div class="progress progress-striped active"> */
/*                                 <div class="bar p80"></div>                                 */
/*                             </div>                             */
/*                             <div class="info"> */
/*                                 <div class="span6 first"> */
/*                                     <i class="icon-user"></i> */
/*                                     <span> 147 </span> Pledgers*/
/*                                 </div>                                 */
/*                                 <div class="span6"> */
/*                                     <i class="icon-calendar-empty"></i> */
/*                                     <span> 204 </span> Days Left*/
/*                                 </div>                                 */
/*                             </div>                             */
/*                         </section>                         */
/*                         <section class="span4 fund_project" id="charity_progress"> */
/*                             <h3> <a href="crowdfunding_detail.html"> Help us for haiti victams </a> </h3> */
/*                             <img src="{{asset('images/crowd_funding_3.jpg')}}" alt="Fund Featured Image" /> */
/*                             <span class="current_collection">  $9,721 </span> */
/*                             <h4> Pledged of $200,000 Goal </h4> */
/*                             <div class="progress progress-striped active"> */
/*                                 <div class="bar p80"></div>                                 */
/*                             </div>                             */
/*                             <div class="info"> */
/*                                 <div class="span6 first"> */
/*                                     <i class="icon-user"></i> */
/*                                     <span> 39 </span> Pledgers*/
/*                                 </div>                                 */
/*                                 <div class="span6"> */
/*                                     <i class="icon-calendar-empty"></i> */
/*                                     <span> 74 </span> Days Left*/
/*                                 </div>                                 */
/*                             </div>                             */
/*                         </section>                         */
/*                         <hr /> */
/*                         <hr /> */
/*                     </section>                     */
/*                 </section>                 */
/*             </section>             */
/*             <!-- End of Charity News & Progress Section -->             */
/*             <!-- Start of Event & Videos -->             */
/*             <section id="events_videos" class="mbtm"> */
/*                 <section class="container-fluid container"> */
/*                     <section class="row-fluid">                          */
/*                         <figure class="span6 first" id="video_slider"> */
/*                             <h2> NOS <span> Evenement a venir&nbsp;</span> </h2> */
/*                             <div class="video_slider_container span8 offset2"> */
/*                                 <ul class="video_slider"> */
/*                                     <li> */
/*                                         <div class="video"> */
/*                                             <iframe src="http://player.vimeo.com/video/1823335?title=0&amp;byline=0&amp;portrait=0" width="376" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>                                             */
/*                                             <div class="tag_line"> Food & Shelter provided to Katrina Victams </div>                                             */
/*                                         </div>                                         */
/*                                     </li>                                     */
/*                                 </ul>                                 */
/*                             </div>                             */
/*                         </figure>*/
/*                         <figure class="span5" id="news_accordion"> */
/*                             <div class="accordion" id="accordion_news"> */
/*                                 <div class="accordion-group"> */
/*                                     <div class="accordion-heading"> */
/*                                         <a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion_news" href="#co"> <span class="datem span3 first"> 05/22 </span> <span class="title span8"> City Marathon 2013 */
/* <span class="location_date"> <span class="location"> <i class="icon-map-marker"></i> London </span> <span class="date"> <i class="icon-time"></i>		May22, 2013	 </span> </span> </span> <span class="span1" id="icon_toggle"> <i class="icon-minus"> </i></span> </a> */
/*                                     </div>                                     */
/*                                     <div id="co" class="accordion-body collapse in"> */
/*                                         <div class="accordion-inner"> */
/*                                             <figure class="span3 img first"> */
/*                                                 <img src="{{asset('images/accordion_img.jpg')}}" alt="" /> */
/*                                             </figure>                                             */
/*                                             <figure class="span9"> */
/*                                                 <p> City Marathon 2013 alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> */
/*                                                 <a href="#"> Read More</a> */
/*                                             </figure>                                             */
/*                                         </div>                                         */
/*                                     </div>                                     */
/*                                 </div>                                 */
/*                                 <div class="accordion-group"> */
/*                                     <div class="accordion-heading"> */
/*                                         <a class="accordion-toggle inactive" data-toggle="collapse" data-parent="#accordion_news" href="#c2"> <span class="datem span3 first"> 05/27 </span> <span class="title span8"> Kids Fun Gala */
/* <span class="location_date"> <span class="location"> <i class="icon-map-marker"></i> London </span> <span class="date"> <i class="icon-time"></i>		May 27, 2013	 </span> </span> </span> <span class="span1" id="icon_toggle"> <i class="icon-plus"> </i></span> </a> */
/*                                     </div>                                     */
/*                                     <div id="c2" class="accordion-body collapse"> */
/*                                         <div class="accordion-inner"> */
/*                                             <figure class="span3 img first"> */
/*                                                 <img src="{{asset('images/accordion_img.jpg')}}" alt="" /> */
/*                                             </figure>                                             */
/*                                             <figure class="span9"> */
/*                                                 <p> City Marathon 2013 alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> */
/*                                                 <a href="#"> Read More</a> */
/*                                             </figure>                                             */
/*                                         </div>                                         */
/*                                     </div>                                     */
/*                                 </div>                                 */
/*                                 <div class="accordion-group"> */
/*                                     <div class="accordion-heading"> */
/*                                         <a class="accordion-toggle inactive" data-toggle="collapse" data-parent="#accordion_news" href="#c3"> <span class="datem span3 first"> 05/29 </span> <span class="title span8"> Family Festival */
/* <span class="location_date"> <span class="location"> <i class="icon-map-marker"></i> London </span> <span class="date"> <i class="icon-time"></i>		May 29, 2013	 </span> </span> </span> <span class="span1" id="icon_toggle"> <i class="icon-plus"> </i></span> </a> */
/*                                     </div>                                     */
/*                                     <div id="c3" class="accordion-body collapse"> */
/*                                         <div class="accordion-inner"> */
/*                                             <figure class="span3 img first"> */
/*                                                 <img src="{{asset('images/accordion_img.jpg')}}" alt="" /> */
/*                                             </figure>                                             */
/*                                             <figure class="span9"> */
/*                                                 <p> City Marathon 2013 alorum prompta, mel ea sumo semper nusquam. Soluta intellegebat vim id, vix timeam latine electram at. </p> */
/*                                                 <a href="#"> Read More</a> */
/*                                             </figure>                                             */
/*                                         </div>                                         */
/*                                     </div>                                     */
/*                                 </div>                                 */
/*                             </div>                             */
/*                         </figure>                         */
/*                     </section>                     */
/*                 </section>                 */
/*             </section>             */
/*             <!-- End Of Event & Videos -->             */
/*             <!-- Start of Blog & Store -->             */
/*             <section id="blog_store" class="mbtm"> */
/*                 <section class="container-fluid container"> */
/* </section>                 */
/*             </section>             */
/*             <!-- End of Blog & Store -->             */
/*             <!-- Start of Footer -->             */
/*             <footer id="footer" class="mtp"> */
/*                 <!-- Start of Footer 1 -->                 */
/*                 <section class="footer_1"> */
/*                     <section class="container-fluid container"> */
/*                         <section class="row-fluid"> */
/*                             <section id="banner" class="span12 first"> */
/*                                 <div class="inner"> */
/*                                     <div class="span9"> */
/*                                         <h2> Free shipping on all orders over $75 </h2> */
/*                                         <h3> * FREE OVER $125 for international orders </h3> */
/*                                     </div>                                     */
/*                                     <div class="pull-left span3"> */
/* </div>                                     */
/*                                 </div>                                 */
/*                             </section>                             */
/*                             <section class="span12 first" id="footer_main"> */
/*                                 <section class="span3 first widget"> */
/*                                     <h4>GET IN <span> Touch </span> <span class="h-line"></span> </h4> */
/*                                     <form id="contact_form" method="post" action="contact.php"> */
/*                                         <input type="text" value="Name" name="name" required /> */
/*                                         <input type="email" value="Email" name="email" required /> */
/*                                         <textarea rows="5" cols="20" name="comments" required>Comments</textarea>                                         */
/*                                         <input type="submit" name="submit" value="Send Message" /> */
/*                                     </form>                                     */
/*                                 </section>                                 */
/*                                 <section class="span3 widget popular_post"> */
/*                                     <h4>Blog Popular <span> Post </span> <span class="h-line"></span> </h4> */
/*                                     <ul id="popular_post"> */
/*                                         <li> */
/*                                             <span> <i class="icon-facetime-video"></i> </span> */
/*                                             <p> Assum mucius maiestatis et nam purto virtute	 </p>*/
/*                                         </li>                                         */
/*                                         <li> */
/*                                             <span> <i class="icon-volume-up"></i></span> */
/*                                             <p> <a href="#"> Assum mucius maiestatis et nam purto virtute	</a> </p>*/
/*                                         </li>                                         */
/*                                         <li> */
/*                                             <span> <i class="icon-picture"></i> </span> */
/*                                             <p> Assum mucius maiestatis et nam purto virtute	 </p>*/
/*                                         </li>                                         */
/*                                         <li> */
/*                                             <span> <i class="icon-volume-up"></i> </span> */
/*                                             <p> Assum mucius maiestatis et nam purto virtute	 </p>*/
/*                                         </li>                                         */
/*                                     </ul>                                     */
/*                                     <a class="v-a" href="http://themeforest/user/crunchpress/">+ View All <span class="h-line"></span></a> */
/*                                 </section>                                 */
/*                                 <section class="widget span3"> */
/*                                     <h4>Latest Twitter <span> Updates </span> <span class="h-line"></span> </h4> */
/*                                     <ul id="tweets_crunchpress"> */
/*                                         <li> New Theme Called Fine Food ready to rocks! Check this out! */
/*                                             <span> <a href="#"> http://fb.me/90DF91 </a> </span> */
/*                                             <span> - 2 hour ago </span> */
/*                                         </li>                                         */
/*                                         <li> The Church Issues Fixed. I hope update will be ready  soon. Then you guys download it. Before upload new  */
/*                                             <span> <a href="#">http://fb.me/4rtIOlp9  </a> </span> */
/*                                             <span> -1 Day ago </span> */
/*                                         </li>                                         */
/*                                     </ul>                                     */
/*                                     <a class="v-a" href="http://themeforest/user/crunchpress/">+ View All <span class="h-line"></span></a> */
/*                                 </section>                                 */
/*                                 <section class="span3 widget"> */
/*                                     <h4>LATEST EVENTS <span> GALLERY </span> <span class="h-line"></span> </h4> */
/*                                     <ul class="gallery-list gallery_widget"> */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_1.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_3.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_2.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_5.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_3.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_2.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_4.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_4.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_3.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_2.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_4.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_4.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_2.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_5.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_1.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_3.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_2.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_5.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_1.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_3.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_3.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_2.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                         <li class="view_new view-tenth"> */
/*                                             <img class="galler-img" src="{{asset('images/gallery_img_4.jpg')}}" alt=""> */
/*                                             <div class="mask"> */
/*                                                 <a class="image-gal info" rel="prettyPhoto[gallery1]" href="{{asset('images/t_slider_4.jpg')}}" title="">&nbsp;</a> */
/*                                             </div>                                             */
/*                                         </li>                                         */
/*                                     </ul>                                     */
/*                                     <a class="v-a" href="http://themeforest/user/crunchpress/">+ View All <span class="h-line"></span></a> */
/*                                 </section>                                 */
/*                             </section>                             */
/*                         </section>                         */
/*                     </section>                     */
/*                 </section>                 */
/*                 <!-- End of Footer 1 -->                 */
/*                 <!-- Start of Footer 2 -->                 */
/*                 <section class="footer_2"> */
/*                     <section class="container-fluid container"> */
/*                         <section class="row-fluid"> */
/*                             <figure class="span6" id="footer_left"> */
/*                                 <i class="icon-mobile-phone"></i> */
/*                                 <span> +00 71 900 34 45 </span> */
/*                                 <i class="icon-envelope-alt"></i> */
/*                                 <span> contact@companyname.com  </span> */
/*                             </figure>                             */
/*                             <figure class="span6" id="footer_right"> */
/*                                 <div id="socialicons"> */
/*                                     <a data-placement="top" rel="tooltip" title="Join us on Facebook" id="social_facebook" class="social_active" href="#"> <span> </span></a> */
/*                                     <a data-placement="top" rel="tooltip" title="Follow us on Twitter" id="social_twitter" class="social_active" href="#"> <span> </span></a> */
/*                                     <a data-placement="top" rel="tooltip" title="Visit LinkedIn Page" id="social_linkedin" class="social_active" href="#"> <span> </span></a> */
/*                                     <a data-placement="top" rel="tooltip" title="Browse Flicker Gallery" id="social_flickr" class="social_active" href="#"> <span> </span></a> */
/*                                     <a data-placement="top" rel="tooltip" title="Check us on Forst " id="social_forst" class="social_active" href="#"> <span> </span></a> */
/*                                     <a data-placement="top" rel="tooltip" title="View Viemeo Video Collection" id="social_vimeo" class="social_active"> <span> </span></a> */
/*                                     <a data-placement="top" rel="tooltip" title="Pin Us" id="social_pinterest" class="social_active" href="#"> <span></span> </a> */
/*                                     <a data-placement="top" rel="tooltip" title="Join our Social Circle" id="social_google_plus" class="social_active"> <span> </span></a> */
/*                                     <a data-placement="top" rel="tooltip" title="Watch Our Broadcasting" id="social_youtube" class="social_active" href="#"> <span> </span></a> */
/*                                 </div>                                 */
/*                             </figure>                             */
/*                         </section>                         */
/*                     </section>                     */
/*                 </section>   */
/* */
/* {% endblock %}        */
