<?php

/* EasyAdminBundle:default:label_undefined.html.twig */
class __TwigTemplate_16288659c84873f962f2082f9544d072be2b8734af7cd19503477aaa5d126551 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_undefined.html.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }
}
/* {# this template is rendered when an error or exception occurs while*/
/*    trying to get the value of the field (by default nothing is displayed) #}*/
/* */
