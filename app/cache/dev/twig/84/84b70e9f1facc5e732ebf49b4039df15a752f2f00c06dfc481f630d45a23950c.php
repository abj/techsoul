<?php

/* ::base.html.twig */
class __TwigTemplate_4267999be47da950e2ea75738202252870f3363e4aae298b8a6f11ccf773fa7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
                <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width\">
";
        // line 12
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "48ed673_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_48ed673_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/48ed673_comments_1.css");
            // line 13
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
            echo "\">
        <link rel=\"stylesheet\" href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/icomoon-social.css"), "html", null, true);
            echo "\">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

        <link rel=\"stylesheet\" href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.css"), "html", null, true);
            echo "\" />
\t\t<!--[if lte IE 8]>
\t\t    <link rel=\"stylesheet\" href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.ie.css"), "html", null, true);
            echo "\" />
\t\t<![endif]-->
\t\t<link rel=\"stylesheet\" href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main.css"), "html", null, true);
            echo "\">

        <script src=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
            echo "\"></script>
        <script src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"), "html", null, true);
            echo "\"></script>
";
        } else {
            // asset "48ed673"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_48ed673") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/48ed673.css");
            // line 13
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
            echo "\">
        <link rel=\"stylesheet\" href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/icomoon-social.css"), "html", null, true);
            echo "\">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

        <link rel=\"stylesheet\" href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.css"), "html", null, true);
            echo "\" />
\t\t<!--[if lte IE 8]>
\t\t    <link rel=\"stylesheet\" href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.ie.css"), "html", null, true);
            echo "\" />
\t\t<![endif]-->
\t\t<link rel=\"stylesheet\" href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main.css"), "html", null, true);
            echo "\">

        <script src=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
            echo "\"></script>
        <script src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"), "html", null, true);
            echo "\"></script>
";
        }
        unset($context["asset_url"]);
        // line 26
        echo "        ";
        $this->displayBlock('css', $context, $blocks);
        // line 28
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        
        
        
        
        
    </head>
    <body>
        ";
        // line 37
        $this->loadTemplate("::navbar.html.twig", "::base.html.twig", 37)->display($context);
        // line 38
        echo "        
        ";
        // line 39
        $this->displayBlock('body', $context, $blocks);
        // line 45
        echo "        
        
        
        
        
        ";
        // line 50
        $this->loadTemplate("::footer.html.twig", "::base.html.twig", 50)->display($context);
        // line 51
        echo "        
        
        ";
        // line 53
        $this->displayBlock('javascripts', $context, $blocks);
        // line 55
        echo "          <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}\"></script>
        <script>window.jQuery || document.write('<script src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.9.1.min.js"), "html", null, true);
        echo "\"><\\/script>')</script>
        <script src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js')}}\"></script>
        <script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.fitvids.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.sequence-min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.bxslider.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/main-menu.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/template.js"), "html", null, true);
        echo "\"></script>
        
         <!-- End Main Wrapper -->         
                                                                <!-- JS Files Start -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/lib-1-9-1.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- lib Js -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/lib-1-7-1.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- lib Js -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- Modernizr -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/easing.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- Easing js -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- Bootstrap -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bxslider.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- BX Slider -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fitvids.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- fIt Video -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/clearInput.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- Input Clear -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/smooth-scroll.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- smooth Scroll -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/prettyPhoto.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- Pretty Photo -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/social.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- Social Media Hover Effect -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/countdown.js"), "html", null, true);
        echo "\"></script>
                                                                <!-- Event Counter -->         
                                                                <script type=\"text/javascript\" src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/custom.js"), "html", null, true);
        echo "\"></script>


                                                                <!--------------------------------------login ------------------>

                                                                <script type=\"text/javascript\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/test/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>
                                                                <script type=\"text/javascript\" src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/test/login.js"), "html", null, true);
        echo "\"></script>


                                                                <script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.11.1.min.js"), "html", null, true);
        echo "\"></script>
                                                                <script src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-migrate-1.2.1.min.js"), "html", null, true);
        echo "\"></script>
                                                                <script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\" ></script>
                                                                <script src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr.min.js"), "html", null, true);
        echo "\"></script>
                                                                <script src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/pace.min.js"), "html", null, true);
        echo "\"></script>
                                                                <script src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/retina.min.js"), "html", null, true);
        echo "\"></script>
                                                                <script src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.cookies.js"), "html", null, true);
        echo "\"></script>

                                                                <script src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
                                                                <script src=\"//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js\"></script>
                                                                <script src=\"//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js\"></script>
                                                                <script src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/select2.min.js"), "html", null, true);
        echo "\"></script>

                                                                <script src=";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/custom.js"), "html", null, true);
        echo "\"\"></script>
                                                            
                                                                <script>
                                                                    jQuery(document).ready(function () {

                                                                        jQuery('#basicTable').DataTable({
                                                                            responsive: true
                                                                        });

                                                                        var shTable = jQuery('#shTable').DataTable({
                                                                            \"fnDrawCallback\": function (oSettings) {
                                                                                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                                                                            },
                                                                            responsive: true
                                                                        });

                                                                        // Show/Hide Columns Dropdown
                                                                        jQuery('#shCol').click(function (event) {
                                                                            event.stopPropagation();
                                                                        });

                                                                        jQuery('#shCol input').on('click', function () {

                                                                            // Get the column API object
                                                                            var column = shTable.column(\$(this).val());

                                                                            // Toggle the visibility
                                                                            if (\$(this).is(':checked'))
                                                                                column.visible(true);
                                                                            else
                                                                                column.visible(false);
                                                                        });

                                                                        var exRowTable = jQuery('#exRowTable').DataTable({
                                                                            responsive: true,
                                                                            \"fnDrawCallback\": function (oSettings) {
                                                                                jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                                                                            },
                                                                            \"ajax\": \"ajax/objects.txt\",
                                                                            \"columns\": [
                                                                                {
                                                                                    \"class\": 'details-control',
                                                                                    \"orderable\": false,
                                                                                    \"data\": null,
                                                                                    \"defaultContent\": ''
                                                                                },
                                                                                {\"data\": \"titre\"},
                                                                                {\"data\": \"continue\"},
                                                                                {\"data\": \"notification\"},
                                                                              
                                                                            ],
                                                                            \"order\": [[1, 'asc']]
                                                                        });

                                                                        // Add event listener for opening and closing details
                                                                        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
                                                                            var tr = \$(this).closest('tr');
                                                                            var row = exRowTable.row(tr);

                                                                            if (row.child.isShown()) {
                                                                                // This row is already open - close it
                                                                                row.child.hide();
                                                                                tr.removeClass('shown');
                                                                            }
                                                                            else {
                                                                                // Open this row
                                                                                row.child(format(row.data())).show();
                                                                                tr.addClass('shown');
                                                                            }
                                                                        });


                                                                        // DataTables Length to Select2
                                                                        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
                                                                        jQuery('div.dataTables_length select').css({width: '60px'});
                                                                        jQuery('div.dataTables_length select').select2({
                                                                            minimumResultsForSearch: -1
                                                                        });

                                                                    });

                                                                    function format(d) {
                                                                        // `d` is the original data object for the row
                                                                        return '<table class=\"table table-bordered nomargin\">' +
                                                                                '<tr>' +
                                                                                '<td>Full name:</td>' +
                                                                                '<td>' + d.name + '</td>' +
                                                                                '</tr>' +
                                                                                '<tr>' +
                                                                                '<td>Extension number:</td>' +
                                                                                '<td>' + d.extn + '</td>' +
                                                                                '</tr>' +
                                                                                '<tr>' +
                                                                                '<td>Extra info:</td>' +
                                                                                '<td>And any further details here (images etc)...</td>' +
                                                                                '</tr>' +
                                                                                '</table>';
                                                                    }
                                                                </script>

                                                               


        
    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        echo "        ";
    }

    // line 39
    public function block_body($context, array $blocks = array())
    {
        // line 40
        echo "        
        
        
        
        ";
    }

    // line 53
    public function block_javascripts($context, array $blocks = array())
    {
        // line 54
        echo "        ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  445 => 54,  442 => 53,  434 => 40,  431 => 39,  427 => 27,  424 => 26,  418 => 5,  307 => 113,  302 => 111,  296 => 108,  291 => 106,  287 => 105,  283 => 104,  279 => 103,  275 => 102,  271 => 101,  267 => 100,  261 => 97,  257 => 96,  249 => 91,  244 => 89,  239 => 87,  234 => 85,  229 => 83,  224 => 81,  219 => 79,  214 => 77,  209 => 75,  204 => 73,  199 => 71,  194 => 69,  189 => 67,  182 => 63,  178 => 62,  174 => 61,  170 => 60,  166 => 59,  161 => 57,  157 => 56,  154 => 55,  152 => 53,  148 => 51,  146 => 50,  139 => 45,  137 => 39,  134 => 38,  132 => 37,  119 => 28,  116 => 26,  110 => 24,  106 => 23,  101 => 21,  96 => 19,  91 => 17,  85 => 14,  80 => 13,  73 => 24,  69 => 23,  64 => 21,  59 => 19,  54 => 17,  48 => 14,  43 => 13,  39 => 12,  29 => 5,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         */
/*                 <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">*/
/*         <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>*/
/*         <meta name="description" content="">*/
/*         <meta name="viewport" content="width=device-width">*/
/* {% stylesheets '@FOSCommentBundle/Resources/assets/css/comments.css' %}*/
/*         <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/icomoon-social.css')}}">*/
/*         <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>*/
/* */
/*         <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" />*/
/* 		<!--[if lte IE 8]>*/
/* 		    <link rel="stylesheet" href="{{asset('css/leaflet.ie.css')}}" />*/
/* 		<![endif]-->*/
/* 		<link rel="stylesheet" href="{{asset('css/main.css')}}">*/
/* */
/*         <script src="{{asset('js/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>*/
/*         <script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js')}}"></script>*/
/* {% endstylesheets %}*/
/*         {% block css %}*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*     </head>*/
/*     <body>*/
/*         {% include '::navbar.html.twig' %}*/
/*         */
/*         {% block body %}*/
/*         */
/*         */
/*         */
/*         */
/*         {% endblock %}*/
/*         */
/*         */
/*         */
/*         */
/*         */
/*         {% include '::footer.html.twig' %}*/
/*         */
/*         */
/*         {% block javascripts %}*/
/*         {% endblock %}*/
/*           <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}"></script>*/
/*         <script>window.jQuery || document.write('<script src="{{asset('js/jquery-1.9.1.min.js')}}"><\/script>')</script>*/
/*         <script src="{{asset('js/bootstrap.min.js')}}"></script>*/
/*         <script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.fitvids.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.sequence-min.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.bxslider.js')}}"></script>*/
/*         <script src="{{asset('js/main-menu.js')}}"></script>*/
/*         <script src="{{asset('js/template.js')}}"></script>*/
/*         */
/*          <!-- End Main Wrapper -->         */
/*                                                                 <!-- JS Files Start -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/lib-1-9-1.js')}}"></script>*/
/*                                                                 <!-- lib Js -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/lib-1-7-1.js')}}"></script>*/
/*                                                                 <!-- lib Js -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/modernizr.js')}}"></script>*/
/*                                                                 <!-- Modernizr -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/easing.js')}}"></script>*/
/*                                                                 <!-- Easing js -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>*/
/*                                                                 <!-- Bootstrap -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/bxslider.js')}}"></script>*/
/*                                                                 <!-- BX Slider -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/fitvids.js')}}"></script>*/
/*                                                                 <!-- fIt Video -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/clearInput.js')}}"></script>*/
/*                                                                 <!-- Input Clear -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/smooth-scroll.js')}}"></script>*/
/*                                                                 <!-- smooth Scroll -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/prettyPhoto.js')}}"></script>*/
/*                                                                 <!-- Pretty Photo -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/social.js')}}"></script>*/
/*                                                                 <!-- Social Media Hover Effect -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/countdown.js')}}"></script>*/
/*                                                                 <!-- Event Counter -->         */
/*                                                                 <script type="text/javascript" src="{{asset('js/custom.js')}}"></script>*/
/* */
/* */
/*                                                                 <!--------------------------------------login ------------------>*/
/* */
/*                                                                 <script type="text/javascript" src="{{asset('js/test/jquery.leanModal.min.js')}}"></script>*/
/*                                                                 <script type="text/javascript" src="{{asset('js/test/login.js')}}"></script>*/
/* */
/* */
/*                                                                 <script src="{{asset('js/jquery-1.11.1.min.js')}}"></script>*/
/*                                                                 <script src="{{asset('js/jquery-migrate-1.2.1.min.js')}}"></script>*/
/*                                                                 <script src="{{asset('js/bootstrap.min.js')}}" ></script>*/
/*                                                                 <script src="{{asset('js/modernizr.min.js')}}"></script>*/
/*                                                                 <script src="{{asset('js/pace.min.js')}}"></script>*/
/*                                                                 <script src="{{asset('js/retina.min.js')}}"></script>*/
/*                                                                 <script src="{{asset('js/jquery.cookies.js')}}"></script>*/
/* */
/*                                                                 <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>*/
/*                                                                 <script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>*/
/*                                                                 <script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>*/
/*                                                                 <script src="{{asset('js/select2.min.js')}}"></script>*/
/* */
/*                                                                 <script src={{asset('js/custom.js')}}""></script>*/
/*                                                             */
/*                                                                 <script>*/
/*                                                                     jQuery(document).ready(function () {*/
/* */
/*                                                                         jQuery('#basicTable').DataTable({*/
/*                                                                             responsive: true*/
/*                                                                         });*/
/* */
/*                                                                         var shTable = jQuery('#shTable').DataTable({*/
/*                                                                             "fnDrawCallback": function (oSettings) {*/
/*                                                                                 jQuery('#shTable_paginate ul').addClass('pagination-active-dark');*/
/*                                                                             },*/
/*                                                                             responsive: true*/
/*                                                                         });*/
/* */
/*                                                                         // Show/Hide Columns Dropdown*/
/*                                                                         jQuery('#shCol').click(function (event) {*/
/*                                                                             event.stopPropagation();*/
/*                                                                         });*/
/* */
/*                                                                         jQuery('#shCol input').on('click', function () {*/
/* */
/*                                                                             // Get the column API object*/
/*                                                                             var column = shTable.column($(this).val());*/
/* */
/*                                                                             // Toggle the visibility*/
/*                                                                             if ($(this).is(':checked'))*/
/*                                                                                 column.visible(true);*/
/*                                                                             else*/
/*                                                                                 column.visible(false);*/
/*                                                                         });*/
/* */
/*                                                                         var exRowTable = jQuery('#exRowTable').DataTable({*/
/*                                                                             responsive: true,*/
/*                                                                             "fnDrawCallback": function (oSettings) {*/
/*                                                                                 jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');*/
/*                                                                             },*/
/*                                                                             "ajax": "ajax/objects.txt",*/
/*                                                                             "columns": [*/
/*                                                                                 {*/
/*                                                                                     "class": 'details-control',*/
/*                                                                                     "orderable": false,*/
/*                                                                                     "data": null,*/
/*                                                                                     "defaultContent": ''*/
/*                                                                                 },*/
/*                                                                                 {"data": "titre"},*/
/*                                                                                 {"data": "continue"},*/
/*                                                                                 {"data": "notification"},*/
/*                                                                               */
/*                                                                             ],*/
/*                                                                             "order": [[1, 'asc']]*/
/*                                                                         });*/
/* */
/*                                                                         // Add event listener for opening and closing details*/
/*                                                                         jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {*/
/*                                                                             var tr = $(this).closest('tr');*/
/*                                                                             var row = exRowTable.row(tr);*/
/* */
/*                                                                             if (row.child.isShown()) {*/
/*                                                                                 // This row is already open - close it*/
/*                                                                                 row.child.hide();*/
/*                                                                                 tr.removeClass('shown');*/
/*                                                                             }*/
/*                                                                             else {*/
/*                                                                                 // Open this row*/
/*                                                                                 row.child(format(row.data())).show();*/
/*                                                                                 tr.addClass('shown');*/
/*                                                                             }*/
/*                                                                         });*/
/* */
/* */
/*                                                                         // DataTables Length to Select2*/
/*                                                                         jQuery('div.dataTables_length select').removeClass('form-control input-sm');*/
/*                                                                         jQuery('div.dataTables_length select').css({width: '60px'});*/
/*                                                                         jQuery('div.dataTables_length select').select2({*/
/*                                                                             minimumResultsForSearch: -1*/
/*                                                                         });*/
/* */
/*                                                                     });*/
/* */
/*                                                                     function format(d) {*/
/*                                                                         // `d` is the original data object for the row*/
/*                                                                         return '<table class="table table-bordered nomargin">' +*/
/*                                                                                 '<tr>' +*/
/*                                                                                 '<td>Full name:</td>' +*/
/*                                                                                 '<td>' + d.name + '</td>' +*/
/*                                                                                 '</tr>' +*/
/*                                                                                 '<tr>' +*/
/*                                                                                 '<td>Extension number:</td>' +*/
/*                                                                                 '<td>' + d.extn + '</td>' +*/
/*                                                                                 '</tr>' +*/
/*                                                                                 '<tr>' +*/
/*                                                                                 '<td>Extra info:</td>' +*/
/*                                                                                 '<td>And any further details here (images etc)...</td>' +*/
/*                                                                                 '</tr>' +*/
/*                                                                                 '</table>';*/
/*                                                                     }*/
/*                                                                 </script>*/
/* */
/*                                                                */
/* */
/* */
/*         */
/*     </body>*/
/* </html>*/
/* */
