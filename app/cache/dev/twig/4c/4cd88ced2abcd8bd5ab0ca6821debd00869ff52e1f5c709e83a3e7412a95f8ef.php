<?php

/* utilisateurBundle:admin:afficherUserAdmin.html.twig */
class __TwigTemplate_86323b2b010d7f2b6e87911bde8815b304aaf479ea4f844a404b16120585a208 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin/layoutAdmin.html.twig", "utilisateurBundle:admin:afficherUserAdmin.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin/layoutAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
    <!-- Tasks table -->
    <div class=\"block\">
        <h6 class=\"heading-hr\"><i class=\"icon-grid\"></i> Recent tasks</h6>
        <div class=\"datatable-tasks\">
            <table class=\"table table-bordered\">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th class=\"task-priority\">Nom</th>
                        <th class=\"task-date-added\">Prenom</th>
                        <th class=\"task-progress\">Email</th>
                        <th class=\"task-deadline\">derniere connexion</th>
                        <th class=\"task-tools text-center\">Tools</th>
                    </tr>
                </thead>
                <tbody>
                                ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 21
            echo "
                    <tr>
                        <td class=\"task-desc\">
                            <a href=\"\"> ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</span>
                        </td>
                        <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "username", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "Password", array()), "html", null, true);
            echo "</td>
                        <td>
                            ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "email", array()), "html", null, true);
            echo "
                            </div>
                        </td>
                        <td><i class=\"icon-clock\"></i> <strong class=\"text-danger\">";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "lastlogin", array()), "m/d/Y"), "html", null, true);
            echo "</strong> hours left</td>
                        <td class=\"text-center\">
                            <div class=\"btn-group\">
                                <button type=\"button\" class=\"btn btn-icon btn-success dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-cog4\"></i></button>
                                <ul class=\"dropdown-menu icons-right dropdown-menu-right\">
                                    <li><a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\"><i class=\"icon-quill2\"></i> voir profile </a></li>
                                    <li><a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\"><i class=\"icon-share2\"></i> editer profile</a></li>
                                    <li><a href=\"#\"><i class=\"icon-checkmark3\"></i> Complete</a></li>
                                    <li><a href=\"#\"><i class=\"icon-stack\"></i> Archive</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>  
                                </tbody>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "                                    
            </table>
        </div>
    </div>
    <!-- /tasks table -->

    <ul>
        <li>
            <a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("user_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
            
";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:admin:afficherUserAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 55,  106 => 47,  91 => 38,  87 => 37,  79 => 32,  73 => 29,  68 => 27,  64 => 26,  59 => 24,  54 => 21,  50 => 20,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "::admin/layoutAdmin.html.twig" %}*/
/* {% block body %}*/
/* */
/*     <!-- Tasks table -->*/
/*     <div class="block">*/
/*         <h6 class="heading-hr"><i class="icon-grid"></i> Recent tasks</h6>*/
/*         <div class="datatable-tasks">*/
/*             <table class="table table-bordered">*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <th>ID</th>*/
/*                         <th class="task-priority">Nom</th>*/
/*                         <th class="task-date-added">Prenom</th>*/
/*                         <th class="task-progress">Email</th>*/
/*                         <th class="task-deadline">derniere connexion</th>*/
/*                         <th class="task-tools text-center">Tools</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                                 {% for entity in entities %}*/
/* */
/*                     <tr>*/
/*                         <td class="task-desc">*/
/*                             <a href=""> {{entity.id}}</span>*/
/*                         </td>*/
/*                         <td>{{entity.username}}</td>*/
/*                         <td>{{entity.Password}}</td>*/
/*                         <td>*/
/*                             {{entity.email}}*/
/*                             </div>*/
/*                         </td>*/
/*                         <td><i class="icon-clock"></i> <strong class="text-danger">{{entity.lastlogin|date("m/d/Y")}}</strong> hours left</td>*/
/*                         <td class="text-center">*/
/*                             <div class="btn-group">*/
/*                                 <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>*/
/*                                 <ul class="dropdown-menu icons-right dropdown-menu-right">*/
/*                                     <li><a href="{{ path('user_show', { 'id': entity.id }) }}"><i class="icon-quill2"></i> voir profile </a></li>*/
/*                                     <li><a href="{{ path('user_edit', { 'id': entity.id }) }}"><i class="icon-share2"></i> editer profile</a></li>*/
/*                                     <li><a href="#"><i class="icon-checkmark3"></i> Complete</a></li>*/
/*                                     <li><a href="#"><i class="icon-stack"></i> Archive</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </td>*/
/*                     </tr>  */
/*                                 </tbody>*/
/*                                 {% endfor %}*/
/*                                     */
/*             </table>*/
/*         </div>*/
/*     </div>*/
/*     <!-- /tasks table -->*/
/* */
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('user_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/*             */
/* {% endblock %}*/
