<?php

/* crowdBundle:Default/test:test.html.twig */
class __TwigTemplate_42d71db593d8e7221ed3ef49696760eda9e5dfb73c03e9ede05285991208d450 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
     ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["id"]);
        foreach ($context['_seq'] as $context["_key"] => $context["id"]) {
            // line 4
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["id"], "username", array()), "html", null, true);
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['id'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "     
     
    celui qui est connecte est ";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["connecte"]) ? $context["connecte"] : $this->getContext($context, "connecte")), "html", null, true);
    }

    public function getTemplateName()
    {
        return "crowdBundle:Default/test:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 8,  35 => 6,  26 => 4,  22 => 3,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* */
/*      {% for id in id %}*/
/*         <li>{{ id.username }}</li>*/
/*     {% endfor %}*/
/*      */
/*      */
/*     celui qui est connecte est {{connecte}}*/
