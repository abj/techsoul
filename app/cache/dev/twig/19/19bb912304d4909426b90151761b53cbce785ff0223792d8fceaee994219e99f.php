<?php

/* utilisateurBundle:user:mesprojet.html.twig */
class __TwigTemplate_bb3b6a2bdd1b1deb93f21668182b8e33f34beaa44006030008d0137df9679972 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:user:mesprojet.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'css' => array($this, 'block_css'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayBlock('css', $context, $blocks);
        // line 7
        echo "

<div class=\"container\">
    <div class=\"row profile\">
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"profile-sidebar\">
\t\t\t\t<!-- SIDEBAR USERPIC -->
\t\t\t\t<div class=\"profile-userpic\">
\t\t\t\t\t<img src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/test/avatar.png"), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"\">
\t\t\t\t</div>
\t\t\t\t<!-- END SIDEBAR USERPIC -->
\t\t\t\t<!-- SIDEBAR USER TITLE -->
\t\t\t\t<div class=\"profile-usertitle\">
\t\t\t\t\t<div class=\"profile-usertitle-name\">
\t\t\t\t\t";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"profile-usertitle-job\">
\t\t\t\t\t\tDeveloper
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- END SIDEBAR USER TITLE -->
\t\t\t\t<!-- SIDEBAR BUTTONS -->
\t\t\t\t<div class=\"profile-userbuttons\">
\t\t\t\t\t<button type=\"button\" class=\"btn btn-success btn-sm\">Follow</button>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger btn-sm\">Message</button>
\t\t\t\t</div>
\t\t\t\t<!-- END SIDEBAR BUTTONS -->
\t\t\t\t<!-- SIDEBAR MENU -->
\t\t\t\t<div class=\"profile-usermenu\">
\t\t\t\t\t<ul class=\"nav\">
\t\t\t\t\t\t<li class=\"active\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-home\"></i>
\t\t\t\t\t\t\tOverview </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
        echo "\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-user\"></i>
\t\t\t\t\t\t\tparametre du compte  </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("mesprojet");
        echo "\" target=\"_blank\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-ok\"  ></i>
\t\t\t\t\t\t\tmes projett </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-flag\"></i>
\t\t\t\t\t\t\tHelp </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<!-- END MENU -->
\t\t\t</div>
\t\t</div>
                                        <div class=\"col-md-9\">
                                            <div class=\"profile-content\">

                                            
                                                  <h1>Projet list</h1>

    <table class=\"records_list\">
        <thead>
            <tr>
                <th>Idprojet</th>
                <th>Nomprojet</th>
                <th>Resume</th>
                <th>Image</th>
                <th>Budjet</th>
                <th>Fichier</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 82
            echo "            <tr>
                <td><a href=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_show", array("id" => $this->getAttribute($context["entity"], "idProjet", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "idProjet", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nomProjet", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "resume", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "image", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "budjet", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "fichier", array()), "html", null, true);
            echo "</td>
                <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 92
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_show", array("id" => $this->getAttribute($context["entity"], "idProjet", array()))), "html", null, true);
            echo "\">show</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 95
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_edit", array("id" => $this->getAttribute($context["entity"], "idProjet", array()))), "html", null, true);
            echo "\">edit</a>
                    </li>
                </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "        </tbody>
    </table>

        <ul>
        <li>
            <a href=\"";
        // line 106
        echo $this->env->getExtension('routing')->getPath("projet_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
                                            
                                            </div>
\t</div>
</div>

<br>
<br>
";
    }

    // line 4
    public function block_css($context, array $blocks = array())
    {
        // line 5
        echo "                <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/test/test.css"), "html", null, true);
        echo "\">
                ";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:user:mesprojet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 5,  201 => 4,  184 => 106,  177 => 101,  165 => 95,  159 => 92,  152 => 88,  148 => 87,  144 => 86,  140 => 85,  136 => 84,  130 => 83,  127 => 82,  123 => 81,  87 => 48,  79 => 43,  54 => 21,  45 => 15,  35 => 7,  32 => 4,  29 => 3,  11 => 2,);
    }
}
/* */
/*     {% extends "::base.html.twig" %}*/
/* {% block body %}*/
/*     {% block css %}*/
/*                 <link rel="stylesheet" href="{{asset('css/test/test.css')}}">*/
/*                 {% endblock %}*/
/* */
/* */
/* <div class="container">*/
/*     <div class="row profile">*/
/* 		<div class="col-md-3">*/
/* 			<div class="profile-sidebar">*/
/* 				<!-- SIDEBAR USERPIC -->*/
/* 				<div class="profile-userpic">*/
/* 					<img src="{{asset('css/test/avatar.png')}}" class="img-responsive" alt="">*/
/* 				</div>*/
/* 				<!-- END SIDEBAR USERPIC -->*/
/* 				<!-- SIDEBAR USER TITLE -->*/
/* 				<div class="profile-usertitle">*/
/* 					<div class="profile-usertitle-name">*/
/* 					{{ app.user.username }}*/
/* 					</div>*/
/* 					<div class="profile-usertitle-job">*/
/* 						Developer*/
/* 					</div>*/
/* 				</div>*/
/* 				<!-- END SIDEBAR USER TITLE -->*/
/* 				<!-- SIDEBAR BUTTONS -->*/
/* 				<div class="profile-userbuttons">*/
/* 					<button type="button" class="btn btn-success btn-sm">Follow</button>*/
/* 					<button type="button" class="btn btn-danger btn-sm">Message</button>*/
/* 				</div>*/
/* 				<!-- END SIDEBAR BUTTONS -->*/
/* 				<!-- SIDEBAR MENU -->*/
/* 				<div class="profile-usermenu">*/
/* 					<ul class="nav">*/
/* 						<li class="active">*/
/* 							<a href="#">*/
/* 							<i class="glyphicon glyphicon-home"></i>*/
/* 							Overview </a>*/
/* 						</li>*/
/* 						<li>*/
/* 							<a href="{{path('fos_user_profile_edit')}}">*/
/* 							<i class="glyphicon glyphicon-user"></i>*/
/* 							parametre du compte  </a>*/
/* 						</li>*/
/* 						<li>*/
/* 							<a href="{{path('mesprojet')}}" target="_blank">*/
/* 							<i class="glyphicon glyphicon-ok"  ></i>*/
/* 							mes projett </a>*/
/* 						</li>*/
/* 						<li>*/
/* 							<a href="#">*/
/* 							<i class="glyphicon glyphicon-flag"></i>*/
/* 							Help </a>*/
/* 						</li>*/
/* 					</ul>*/
/* 				</div>*/
/* 				<!-- END MENU -->*/
/* 			</div>*/
/* 		</div>*/
/*                                         <div class="col-md-9">*/
/*                                             <div class="profile-content">*/
/* */
/*                                             */
/*                                                   <h1>Projet list</h1>*/
/* */
/*     <table class="records_list">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>Idprojet</th>*/
/*                 <th>Nomprojet</th>*/
/*                 <th>Resume</th>*/
/*                 <th>Image</th>*/
/*                 <th>Budjet</th>*/
/*                 <th>Fichier</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for entity in entities %}*/
/*             <tr>*/
/*                 <td><a href="{{ path('projet_show', { 'id': entity.idProjet }) }}">{{ entity.idProjet }}</a></td>*/
/*                 <td>{{ entity.nomProjet }}</td>*/
/*                 <td>{{ entity.resume }}</td>*/
/*                 <td>{{ entity.image }}</td>*/
/*                 <td>{{ entity.budjet }}</td>*/
/*                 <td>{{ entity.fichier }}</td>*/
/*                 <td>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <a href="{{ path('projet_show', { 'id': entity.idProjet }) }}">show</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ path('projet_edit', { 'id': entity.idProjet }) }}">edit</a>*/
/*                     </li>*/
/*                 </ul>*/
/*                 </td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul>*/
/*         <li>*/
/*             <a href="{{ path('projet_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/*                                             */
/*                                             </div>*/
/* 	</div>*/
/* </div>*/
/* */
/* <br>*/
/* <br>*/
/* {% endblock %}*/
/* */
