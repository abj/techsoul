<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_f0d34a8fd4235261e3f4bdab12e4b507ab83ee21d1fa8262645c09c879596395 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "FOSUserBundle:Profile:show_content.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'css' => array($this, 'block_css'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayBlock('css', $context, $blocks);
        // line 6
        echo "

<div class=\"container\">
    <div class=\"row profile\">
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"profile-sidebar\">
\t\t\t\t<!-- SIDEBAR USERPIC -->
\t\t\t\t<div class=\"profile-userpic\">
\t\t\t\t\t<img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/test/avatar.png"), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"\">
\t\t\t\t</div>
\t\t\t\t<!-- END SIDEBAR USERPIC -->
\t\t\t\t<!-- SIDEBAR USER TITLE -->
\t\t\t\t<div class=\"profile-usertitle\">
\t\t\t\t\t<div class=\"profile-usertitle-name\">
\t\t\t\t\t";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"profile-usertitle-job\">
\t\t\t\t\t\tDeveloper
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- END SIDEBAR USER TITLE -->
\t\t\t\t<!-- SIDEBAR BUTTONS -->
\t\t\t\t<div class=\"profile-userbuttons\">
\t\t\t\t\t<button type=\"button\" class=\"btn btn-success btn-sm\">Follow</button>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger btn-sm\">Message</button>
\t\t\t\t</div>
\t\t\t\t<!-- END SIDEBAR BUTTONS -->
\t\t\t\t<!-- SIDEBAR MENU -->
\t\t\t\t<div class=\"profile-usermenu\">
\t\t\t\t\t<ul class=\"nav\">
\t\t\t\t\t\t<li class=\"active\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-home\"></i>
\t\t\t\t\t\t\tOverview </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
        echo "\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-user\"></i>
\t\t\t\t\t\t\tparametre du compte  </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("mesprojet");
        echo "\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-ok\"  ></i>
\t\t\t\t\t\t\tmes projett </a>
\t\t\t\t\t\t</li>
                                                <li>
\t\t\t\t\t\t\t<a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("mesMessage");
        echo "\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon glyphicon-envelope\"  ></i>
\t\t\t\t\t\t\tmes message </a>
\t\t\t\t\t\t</li>
                                                <li>
\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon glyphicon-envelope\"  ></i>
\t\t\t\t\t\t\tcomposer message </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-flag\"></i>
\t\t\t\t\t\t\tHelp </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<!-- END MENU -->
\t\t\t</div>
\t\t</div>
                                        <div class=\"col-md-9\">
                                            <div class=\"profile-content\">

                                            
                                            
                                            </div>
\t</div>
</div>

<br>
<br>
";
    }

    // line 3
    public function block_css($context, array $blocks = array())
    {
        // line 4
        echo "                <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/test/test.css"), "html", null, true);
        echo "\">
                ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 4,  130 => 3,  95 => 52,  87 => 47,  79 => 42,  54 => 20,  45 => 14,  35 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* {% block body %}*/
/*     {% block css %}*/
/*                 <link rel="stylesheet" href="{{asset('css/test/test.css')}}">*/
/*                 {% endblock %}*/
/* */
/* */
/* <div class="container">*/
/*     <div class="row profile">*/
/* 		<div class="col-md-3">*/
/* 			<div class="profile-sidebar">*/
/* 				<!-- SIDEBAR USERPIC -->*/
/* 				<div class="profile-userpic">*/
/* 					<img src="{{asset('css/test/avatar.png')}}" class="img-responsive" alt="">*/
/* 				</div>*/
/* 				<!-- END SIDEBAR USERPIC -->*/
/* 				<!-- SIDEBAR USER TITLE -->*/
/* 				<div class="profile-usertitle">*/
/* 					<div class="profile-usertitle-name">*/
/* 					{{ user.username }}*/
/* 					</div>*/
/* 					<div class="profile-usertitle-job">*/
/* 						Developer*/
/* 					</div>*/
/* 				</div>*/
/* 				<!-- END SIDEBAR USER TITLE -->*/
/* 				<!-- SIDEBAR BUTTONS -->*/
/* 				<div class="profile-userbuttons">*/
/* 					<button type="button" class="btn btn-success btn-sm">Follow</button>*/
/* 					<button type="button" class="btn btn-danger btn-sm">Message</button>*/
/* 				</div>*/
/* 				<!-- END SIDEBAR BUTTONS -->*/
/* 				<!-- SIDEBAR MENU -->*/
/* 				<div class="profile-usermenu">*/
/* 					<ul class="nav">*/
/* 						<li class="active">*/
/* 							<a href="#">*/
/* 							<i class="glyphicon glyphicon-home"></i>*/
/* 							Overview </a>*/
/* 						</li>*/
/* 						<li>*/
/* 							<a href="{{path('fos_user_profile_edit')}}">*/
/* 							<i class="glyphicon glyphicon-user"></i>*/
/* 							parametre du compte  </a>*/
/* 						</li>*/
/* 						<li>*/
/* 							<a href="{{path('mesprojet')}}">*/
/* 							<i class="glyphicon glyphicon-ok"  ></i>*/
/* 							mes projett </a>*/
/* 						</li>*/
/*                                                 <li>*/
/* 							<a href="{{path('mesMessage')}}">*/
/* 							<i class="glyphicon glyphicon glyphicon-envelope"  ></i>*/
/* 							mes message </a>*/
/* 						</li>*/
/*                                                 <li>*/
/* 							<a href="">*/
/* 							<i class="glyphicon glyphicon glyphicon-envelope"  ></i>*/
/* 							composer message </a>*/
/* 						</li>*/
/* 						<li>*/
/* 							<a href="#">*/
/* 							<i class="glyphicon glyphicon-flag"></i>*/
/* 							Help </a>*/
/* 						</li>*/
/* 					</ul>*/
/* 				</div>*/
/* 				<!-- END MENU -->*/
/* 			</div>*/
/* 		</div>*/
/*                                         <div class="col-md-9">*/
/*                                             <div class="profile-content">*/
/* */
/*                                             */
/*                                             */
/*                                             </div>*/
/* 	</div>*/
/* </div>*/
/* */
/* <br>*/
/* <br>*/
/* {% endblock %}*/
/* */
