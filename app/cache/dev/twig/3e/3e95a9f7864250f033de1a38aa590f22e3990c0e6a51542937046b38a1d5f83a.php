<?php

/* CCDNForumForumBundle::base.html.twig */
class __TwigTemplate_650e037f193c1e8287efaf0e860471e19b32b22fd011deac342275938f009cbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::forumbase.html.twig", "CCDNForumForumBundle::base.html.twig", 1);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'hn' => array($this, 'block_hn'),
            'sidebar' => array($this, 'block_sidebar'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::forumbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "

";
    }

    // line 8
    public function block_hn($context, array $blocks = array())
    {
        // line 9
        echo "    
    <div class=\"section\">
        <div class=\"container\">
            <div class=\"row\">";
        // line 15
        ob_start();
        // line 17
        echo "<div id=\"page-wrap\">
\t\t\t<div id=\"page-main\">

\t\t\t\t

\t\t\t\t<div class=\"container clearfix\">
\t\t\t\t\t<br>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t        <aside class=\"col-md-2 page-sidebar-left\">";
        // line 26
        $this->displayBlock('sidebar', $context, $blocks);
        // line 28
        echo "</aside>

\t\t\t\t        <div class=\"col-md-10 page-body-right\">
\t\t\t\t\t\t\t<div id=\"top clearfix\"></div>";
        // line 32
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "</div>
\t\t\t        </div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<br>

\t\t<script src=\"//code.jquery.com/jquery.js\"></script>
\t\t<script src=\"//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js\"></script>";
        // line 44
        $this->displayBlock('javascripts', $context, $blocks);
        // line 47
        echo "<script type='text/javascript'>
\t\t<!--
\t\t\t\$(document).ready(function() {
\t\t\t\t// make visible js compat toolbars site wide
\t\t\t\t\$(\"[data-js-non-compat]\").each(function(i, el) {
\t\t\t\t\t\$(el).addClass('collapse').addClass('hidden');
\t\t\t\t});

\t\t\t\t\$(\"[data-js-compat]\").each(function(i, el) {
\t\t\t\t\t\$(el).removeClass('collapse').removeClass('hidden');
\t\t\t\t});

\t\t\t\t\$('[data-tip]').each(function() {
\t\t\t\t\t\$(this).tooltip({
\t\t\t\t\t\thtml: true,
\t\t\t\t\t\tplacement: \$(this).data('tip')
\t\t\t\t\t});
\t\t\t\t});

\t\t\t\t\$('[data-toggle=\"dropdown\"]').dropdown();
\t\t\t});
\t\t//-->
\t\t</script>";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 72
        echo "</div>
            </div>
        </div>
    </div>

    ";
    }

    // line 26
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 32
    public function block_body($context, array $blocks = array())
    {
    }

    // line 44
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "CCDNForumForumBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 44,  122 => 32,  117 => 26,  108 => 72,  83 => 47,  81 => 44,  70 => 34,  68 => 32,  63 => 28,  61 => 26,  51 => 17,  49 => 15,  44 => 9,  41 => 8,  35 => 3,  32 => 2,  11 => 1,);
    }
}
/* {% extends "::forumbase.html.twig" %}*/
/* {% block css %}*/
/* */
/* */
/* {% endblock%}*/
/* */
/* */
/* {% block hn %}*/
/*     */
/*     <div class="section">*/
/*         <div class="container">*/
/*             <div class="row">*/
/* */
/* */
/* 		{%- spaceless -%}*/
/* */
/* 		<div id="page-wrap">*/
/* 			<div id="page-main">*/
/* */
/* 				*/
/* */
/* 				<div class="container clearfix">*/
/* 					<br>*/
/* 					<div class="row">*/
/* 				        <aside class="col-md-2 page-sidebar-left">*/
/* 							{%- block sidebar -%}*/
/* 				            {%- endblock sidebar -%}*/
/* 						</aside>*/
/* */
/* 				        <div class="col-md-10 page-body-right">*/
/* 							<div id="top clearfix"></div>*/
/* 				            {%- block body -%}*/
/* 							{%- endblock body -%}*/
/* 				        </div>*/
/* 			        </div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 		<br>*/
/* */
/* 		<script src="//code.jquery.com/jquery.js"></script>*/
/* 		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>*/
/* 		{%- block javascripts -%}*/
/* 		{%- endblock -%}*/
/* */
/* 		<script type='text/javascript'>*/
/* 		<!--*/
/* 			$(document).ready(function() {*/
/* 				// make visible js compat toolbars site wide*/
/* 				$("[data-js-non-compat]").each(function(i, el) {*/
/* 					$(el).addClass('collapse').addClass('hidden');*/
/* 				});*/
/* */
/* 				$("[data-js-compat]").each(function(i, el) {*/
/* 					$(el).removeClass('collapse').removeClass('hidden');*/
/* 				});*/
/* */
/* 				$('[data-tip]').each(function() {*/
/* 					$(this).tooltip({*/
/* 						html: true,*/
/* 						placement: $(this).data('tip')*/
/* 					});*/
/* 				});*/
/* */
/* 				$('[data-toggle="dropdown"]').dropdown();*/
/* 			});*/
/* 		//-->*/
/* 		</script>*/
/* */
/* 		{%- endspaceless -%}*/
/*                       </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     {%endblock%}*/
/*         */
